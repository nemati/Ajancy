package com.eligasht.reservation.views;

/**
 * Created by Ahmad.nemati on 4/16/2018.
 */
public class TestConst {
    public static final String Email_Forget = "farhadi@eligasht.com";
    public static final String Email = "azizymahsa@gmail.com";
    public static final String Pass = "123456";
    public static final String Origin = "تهران";
    public static final String Dest = "استانبول";
    public static final int Flight_Back = 5;
    public static final int Forgetpassword_Back = 3;
    public static final int LoginAndProfile_Back = 1;
    public static final int Contactus_Back = 1;
    public static final int Aboutus_Back = 1;
    public static final int Terms_Back = 1;

}
