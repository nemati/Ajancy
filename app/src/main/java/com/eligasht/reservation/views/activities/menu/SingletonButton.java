package com.eligasht.reservation.views.activities.menu;

import com.nightonke.boommenu.BoomMenuButton;

/**
 * Created by Ahmad.nemati on 7/25/2018.
 */
public class SingletonButton {
    BoomMenuButton boomMenuButton;
    private static final SingletonButton ourInstance = new SingletonButton();

    public static SingletonButton getInstance() {
        return ourInstance;
    }

    private SingletonButton() {
    }

    public BoomMenuButton getBoomMenuButton() {
        return boomMenuButton;
    }

    public void setBoomMenuButton(BoomMenuButton boomMenuButton) {
        this.boomMenuButton = boomMenuButton;
    }

    public static SingletonButton getOurInstance() {
        return ourInstance;
    }
}
