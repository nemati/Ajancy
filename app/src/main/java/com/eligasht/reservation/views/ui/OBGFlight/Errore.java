package com.eligasht.reservation.views.ui.OBGFlight;

public class Errore {
	
    private int Code ;

    private String DetailedMessage ;

    private String Language ;

    private String Message ;

	public int getCode() {
		return Code;
	}

	public void setCode(int code) {
		Code = code;
	}

	public String getDetailedMessage() {
		return DetailedMessage;
	}

	public void setDetailedMessage(String detailedMessage) {
		DetailedMessage = detailedMessage;
	}

	public String getLanguage() {
		return Language;
	}

	public void setLanguage(String language) {
		Language = language;
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

}
