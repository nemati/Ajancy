package com.eligasht.reservation.views.adapters.hotel.rooms;

/**
 * Created by Reza.nejati on 1/7/2018.
 */

public class ImageModel {
    String image;

    public ImageModel(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
