package com.eligasht.reservation.views.activities.new_login;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.BounceInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.daasuu.ei.Ease;
import com.daasuu.ei.EasingInterpolator;
import com.eligasht.R;
import com.eligasht.reservation.views.activities.menu.MenuActivity;
import com.eligasht.reservation.views.activities.new_login.Fragment.LoginFragment;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class MainActivity extends AppCompatActivity {
    private static final int CUSTOM_OVERLAY_PERMISSION_REQUEST_CODE = 101;
    private static final int CHATHEAD_OVERLAY_PERMISSION_REQUEST_CODE = 100;

    private static int imageResourceIndex = 0;
    Fragment frag_login, frag_dashboard;
    ProgressBar pbar;
    View button_login, button_label, boom, ic_menu1, ic_menu2;
    private DisplayMetrics dm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        pbar = (ProgressBar) findViewById(R.id.mainProgressBar1);

        button_label = findViewById(R.id.button_label);
        dm = getResources().getDisplayMetrics();
        boom = findViewById(R.id.button_icon);
        button_login = findViewById(R.id.button_login);
        button_login.setTag(0);
        pbar.getIndeterminateDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY);
        StatusBarUtil.immersive(this);

        frag_login = new LoginFragment();
        frag_dashboard = new MenuActivity();
        getSupportFragmentManager().beginTransaction().replace(R.id.frag_container, frag_login).commit();
        final ValueAnimator va = new ValueAnimator();
        va.setDuration(750);
        va.setInterpolator(new DecelerateInterpolator());
        va.addUpdateListener(p1 -> {
            RelativeLayout.LayoutParams button_login_lp = (RelativeLayout.LayoutParams) button_login.getLayoutParams();
            button_login_lp.width = Math.round((Float) p1.getAnimatedValue());
            button_login.setLayoutParams(button_login_lp);
        });
        button_login.animate().translationX(dm.widthPixels + button_login.getMeasuredWidth()).setDuration(0).setStartDelay(0).start();
        button_login.animate().translationX(0).setStartDelay(3500).setDuration(1500).setInterpolator(new OvershootInterpolator()).start();
        button_login.setOnClickListener(p1 -> {
            if ((int) button_login.getTag() == 1) {
                return;
            } else if ((int) button_login.getTag() == 2) {
                button_login.animate().x(dm.widthPixels / 2).y(dm.heightPixels / 2).setInterpolator(new EasingInterpolator(Ease.CUBIC_IN)).setListener(null).setDuration(1000).setStartDelay(0).start();
                button_login.animate().setStartDelay(600).setDuration(1000).scaleX(40).scaleY(40).setInterpolator(new EasingInterpolator(Ease.CUBIC_IN_OUT)).start();

                boom.animate().alpha(0).rotation(90).setStartDelay(0).setDuration(800).start();
                return;
            }
            button_login.setTag(1);
            va.setFloatValues(button_login.getMeasuredWidth(), button_login.getMeasuredHeight());
            va.start();
            pbar.animate().setStartDelay(300).setDuration(1000).alpha(1).start();
            button_label.animate().setStartDelay(100).setDuration(500).alpha(0).start();
            button_login.animate().setInterpolator(new FastOutSlowInInterpolator()).setStartDelay(4000).setDuration(1000).scaleX(30).scaleY(30).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator p1) {
                    pbar.animate().setStartDelay(0).setDuration(0).alpha(0).start();
                }

                @Override
                public void onAnimationEnd(Animator p1) {
                    try {
                        getSupportFragmentManager().beginTransaction().replace(R.id.frag_container, frag_dashboard).disallowAddToBackStack().commitAllowingStateLoss();
                    } catch (Exception e) {
                    }
                    button_login.animate().setStartDelay(0).alpha(1).setDuration(1000).scaleX(1).scaleY(1).x(dm.widthPixels - button_login.getMeasuredWidth() - 100).y(dm.heightPixels - button_login.getMeasuredHeight() - 100).setListener(new Animator.AnimatorListener() {

                        @Override
                        public void onAnimationStart(Animator p1) {
                            // TODO: Implement this method
                        }

                        @Override
                        public void onAnimationEnd(Animator p1) {


                            button_login.setVisibility(View.GONE);
                            boom.animate().setDuration(0).setStartDelay(0).rotation(85).alpha(1).start();
                            boom.animate().setDuration(2000).setInterpolator(new BounceInterpolator()).setStartDelay(0).rotation(0).start();
                            button_login.setTag(2);

                        }

                        @Override
                        public void onAnimationCancel(Animator p1) {
                            // TODO: Implement this method
                        }

                        @Override
                        public void onAnimationRepeat(Animator p1) {
                            // TODO: Implement this method
                        }
                    }).start();
                }

                @Override
                public void onAnimationCancel(Animator p1) {
                    // TODO: Implement this method
                }

                @Override
                public void onAnimationRepeat(Animator p1) {
                    // TODO: Implement this method
                }
            }).start();


        });
    }


    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }


}
