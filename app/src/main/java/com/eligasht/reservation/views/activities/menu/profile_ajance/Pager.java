package com.eligasht.reservation.views.activities.menu.profile_ajance;
import android.support.v4.app.Fragment;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class Pager  extends FragmentPagerAdapter {

    //integer to count number of tabs
    int tabCount;

    private AccountAjanceTab acountAjance;
    private ManagerUserTab manageUser;
    private AttachTab attach;

    //Constructor to the class
    public Pager(FragmentManager fm, int tabCount) {
        super(fm);
        //Initializing tab count
        this.tabCount= tabCount;



    }

    //Overriding method getItem
    @Override
    public Fragment getItem(int position) {
        //Returning the current tabs

        switch (position) {
            case 2:
                acountAjance = AccountAjanceTab.instance();
                return acountAjance;
            case 1:
                manageUser = ManagerUserTab.instance();
                return manageUser;
            case 0:
                attach = AttachTab.instance();
                return attach;

            default:
                acountAjance = AccountAjanceTab.instance();
                return acountAjance;
        }
    }

    //Overriden method getCount to get the number of tabs
    @Override
    public int getCount() {
        return tabCount;
    }
}