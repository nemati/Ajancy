package com.eligasht.reservation.views.picker.global.enums;

/**
 * Created by Ahmad.nemati on 3/6/2018.
 */

public enum TypeUsageOfCalendar {
    HOTEL,
    NationalFlight,
    InternationalFlight,
    Train,
    AutoAlert

}
