package com.eligasht.reservation.views.ui.OBGFlight;

public class Warning {
    private int Code ;

    private String Language ;

    private String RecordID ;

    private String ShortText ;

	public int getCode() {
		return Code;
	}

	public void setCode(int code) {
		Code = code;
	}

	public String getLanguage() {
		return Language;
	}

	public void setLanguage(String language) {
		Language = language;
	}

	public String getRecordID() {
		return RecordID;
	}

	public void setRecordID(String recordID) {
		RecordID = recordID;
	}

	public String getShortText() {
		return ShortText;
	}

	public void setShortText(String shortText) {
		ShortText = shortText;
	}
}


