package com.eligasht.reservation.views.activities.menu.contractMenu;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.ramotion.foldingcell.animations.AnimationEndListener;
import com.ramotion.foldingcell.animations.FoldAnimation;
import com.ramotion.foldingcell.animations.HeightAnimation;
import com.ramotion.foldingcell.views.FoldingCellView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Very first implementation of Folding Cell by Ramotion for Android platform
 */
public class FoldingCell {

    // state variables
    private boolean mUnfolded;
    private boolean mAnimationInProgress;

    // default values
    private final int DEF_ANIMATION_DURATION = 1000;
    private final int DEF_BACK_SIDE_COLOR = Color.GRAY;
    private final int DEF_ADDITIONAL_FLIPS = 0;
    private final int DEF_CAMERA_HEIGHT = 30;

    // current settings
    private int mAnimationDuration = DEF_ANIMATION_DURATION;
    private int mBackSideColor = DEF_BACK_SIDE_COLOR;
    private int mAdditionalFlipsCount = DEF_ADDITIONAL_FLIPS;
    private int mCameraHeight = DEF_CAMERA_HEIGHT;



}
