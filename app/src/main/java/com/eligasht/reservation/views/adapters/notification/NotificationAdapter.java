package com.eligasht.reservation.views.adapters.notification;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.eligasht.R;
import com.eligasht.reservation.models.db.NotificationModel;
import com.eligasht.reservation.views.picker.global.model.CustomDate;

import java.util.List;
/**
 * Created by Reza Nejati on 22,May,2018
 */
public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    private final List<NotificationModel> data;
    private Context context;

    public NotificationAdapter(final List<NotificationModel> data) {
        this.data = data;

    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        this.context = parent.getContext();
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.recycler_view_list_notify, parent, false));
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final NotificationModel item = data.get(position);
        holder.setIsRecyclable(false);
        holder.tvTitle.setText(item.getTitle());
        holder.tvBody.setText(item.getBody());
        holder.tvDate.setText(CustomDate.generateLongToStringPersian(item.getDate()));



    }


    @Override
    public int getItemCount() {
        return data.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTitle, tvBody,tvDate;



        public ViewHolder(View v) {
            super(v);
            tvTitle = v.findViewById(R.id.tvTitle);
            tvBody = v.findViewById(R.id.tvBody);
            tvDate = v.findViewById(R.id.tvDate);

        }
    }

}
