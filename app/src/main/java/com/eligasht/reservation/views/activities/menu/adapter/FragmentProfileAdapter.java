package com.eligasht.reservation.views.activities.menu.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.eligasht.reservation.views.activities.menu.profile_ajance.MainProfileAjancFragment;
import com.eligasht.reservation.views.activities.menu.profile_me.MyProfileFragment;

/**
 * Created by Ahmad.nemati on 7/22/2018.
 */
public class FragmentProfileAdapter extends FragmentPagerAdapter {
    public FragmentProfileAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 1:
                return new MainProfileAjancFragment();
            case 0:
                return new MyProfileFragment();
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "پروفایل من";
            case 1:
                return "پروفایل آژانس";
        }

        return "";
    }

    @Override
    public int getCount() {
        return 2;
    }
}
