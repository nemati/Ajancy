package com.eligasht.reservation.views.activities.menu.adapter;

import android.app.Activity;

import android.content.Context;

import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.TextView;

import com.eligasht.R;
import com.eligasht.reservation.views.activities.AboutActivity;
import com.eligasht.reservation.views.activities.main.MainActivity;
import com.eligasht.reservation.views.activities.menu.MenuActivity;
import com.eligasht.reservation.views.activities.menu.MenuModel;
import com.eligasht.reservation.views.activities.menu.contractMenu.MainContractFragment;
import com.eligasht.reservation.views.activities.menu.profile_ajance.AttachTab;
import com.eligasht.reservation.views.ui.GetCountriesForInsuranceActivity;

import java.util.List;

import mehdi.sakout.fancybuttons.FancyButton;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewHolder>{

    private List<MenuModel> countries;
    private int rowLayout;
    private Context mContext;
    private static final int PHOTO_ANIMATION_DELAY = 600;
    private static final Interpolator INTERPOLATOR = new DecelerateInterpolator();


    public MenuAdapter(List<MenuModel> countries, int rowLayout, Context context) {
        this.countries = countries;
        this.rowLayout = rowLayout;
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(rowLayout, viewGroup, false);
        return new ViewHolder(v);
    }



    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        MenuModel menuModel = countries.get(i);
        viewHolder.countryName.setText((CharSequence) menuModel.name);
       // viewHolder.countryImage.setText(mContext.getDrawable(menuModel.getImageResourceId(mContext)));
        viewHolder.countryImage.setText((String) menuModel.imageName);
        long animationDelay = PHOTO_ANIMATION_DELAY + viewHolder.getPosition() * 30;
        viewHolder.itemView.setScaleY(0);
        viewHolder.itemView.setScaleX(0);

        ViewCompat.animate(viewHolder.itemView)
                .scaleY(1)
                .scaleX(1)
                .setDuration(200)
                .setInterpolator(INTERPOLATOR)
                .setStartDelay(animationDelay)
                .start();
        Log.e("animate", "started");

        viewHolder.countryImage.setOnClickListener((View v) -> {

            switch (countries.get(i).name)
            {
                case "جستجوی پرواز":

                    mContext.startActivity(new Intent(mContext, MainActivity.class).putExtra("Type",1));
                break;

                case "جستجوی بلیط هواپیما+رزرو هتل":

                    mContext.startActivity(new Intent(mContext, MainActivity.class).putExtra("Type",2));
                    break;

                case "جستجوی تور":

                    mContext.startActivity(new Intent(mContext, MainActivity.class).putExtra("Type",3));
                    break;

                case "جستجوی هتل":

                    mContext.startActivity(new Intent(mContext, MainActivity.class).putExtra("Type",4));
                    break;

                case "جستجوی بیمه":

                    mContext.startActivity(new Intent(mContext, MainActivity.class).putExtra("Type",5));
                    break;
                    case "قراردادها":
                        mContext.startActivity(new Intent(mContext, MainContractFragment.class));
                      /*  //MainContractFragment   mainContractFragment = MainContractFragment.instance();
                      //  new Handler().postDelayed(this::closeDrawer, 200);
                        // During initial setup, plug in the details fragment.
                        MainContractFragment details = new MainContractFragment();
                        //details.setArguments(getIntent().getExtras());
                        FragmentManager manager = ((Activity) mContext).getFragmentManager();

                      // MenuActivity fragment = new MenuActivity();

                        FragmentTransaction ft = manager.beginTransaction();
                        ft.replace(R.id.layoutmain, details);
                        ft.commit();*/
                    break;
            }
        });

    }

    @Override
    public int getItemCount() {
        return countries == null ? 0 : countries.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView countryName;
        public FancyButton countryImage;

        public ViewHolder(View itemView) {
            super(itemView);
             countryImage = (FancyButton) itemView.findViewById(R.id.imageView);
            //countryImage.setOnClickListener(this);
            countryImage.setCustomTextFont("fonts/fontastic.ttf");
            countryImage.setTextColor(Color.parseColor("#FFF37B20"));
            //countryImage.setText(getString(R.string.icon_dashbord_one));


            countryName = (TextView) itemView.findViewById(R.id.textView);
           // countryImage = (ImageView)itemView.findViewById(R.id.imageView);
        }

    }
}