package com.eligasht.reservation.views.activities.new_login;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PointF;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.TypedValue;

import com.eligasht.R;
import com.eligasht.reservation.views.activities.menu.MainProfileFragment;
import com.eligasht.reservation.views.activities.menu.contractMenu.MainContractFragment;
import com.eligasht.reservation.views.ui.SingletonContext;
import com.nightonke.boommenu.BoomButtons.TextOutsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.Util;

/**
 * Created by Ahmad.nemati on 7/25/2018.
 */
public class CustomBoomButton extends BoomMenuButton {
    private Context context;

    public CustomBoomButton(Context context) {
        super(context);
        this.context = context;
        initBoom();
    }

    public CustomBoomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initBoom();
    }

    public CustomBoomButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initBoom();
    }


    private void initBoom() {
        if (isInEditMode()) return;

        setNormalColor(Color.parseColor("#FFF37B20"));
        setDraggable(true);
        setShowDelay(50);
        setHideDelay(50);
        setDelay(50);
        addBuilder(getTextOutsideCircleButtonBuilderWithDifferentPieceColor(" پرواز", 1, R.drawable.plane,"#FFFC5722"));
        addBuilder(getTextOutsideCircleButtonBuilderWithDifferentPieceColor(" هتل", 4, R.drawable.hotels,"#00bbd4"));
        addBuilder(getTextOutsideCircleButtonBuilderWithDifferentPieceColor("پکیج", 2, R.drawable.packages,"#FF9B28B0"));
        addBuilder(getTextOutsideCircleButtonBuilderWithDifferentPieceColor(" تور", 3, R.drawable.tour,"#FFFD9601"));
        addBuilder(getTextOutsideCircleButtonBuilderWithDifferentPieceColor(" بیمه", 5, R.drawable.insurance,"#FF673BB6"));
        addBuilder(getTextOutsideCircleButtonBuilderWithDifferentPieceColor("قطار خارجی", 0, R.drawable.train,"#FF009687"));
        addBuilder(getTextOutsideCircleButtonBuilderWithDifferentPieceColor("گزارشات", 0, R.drawable.report,"#FFE91E63"));
        addBuilder(getTextOutsideCircleButtonBuilderWithDifferentPieceColor("فروش خدمات", 0, R.drawable.seller,"#FFFC5722"));
        addBuilder(getTextOutsideCircleButtonBuilderWithDifferentPieceColor("قراردادها", 9, R.drawable.contract,"#FF03A8F4"));
        addBuilder(getTextOutsideCircleButtonBuilderWithDifferentPieceColor("حسابداری", 0, R.drawable.accounting,"#FF3F51B4"));
        addBuilder(getTextOutsideCircleButtonBuilderWithDifferentPieceColor("اصلاحیه ها", 0, R.drawable.checklist,"#FF4CAF50"));
        addBuilder(getTextOutsideCircleButtonBuilderWithDifferentPieceColor("پروفایل", 10, R.drawable.user_profile,"#FFFD9601"));


        float w = Util.dp2px(80);
        float h = Util.dp2px(96);
        if (getResources().getBoolean(R.bool.isTablet)) {
            w = (float) (w * 1.5);
            h = (float) (h * 1.5);
        }
        float h_0_5 = h / 2;
        float h_1_5 = h * 1.5f;


        float hm = getButtonHorizontalMargin();
        float vm = getButtonVerticalMargin();
        if (getResources().getBoolean(R.bool.isTablet)) {
            hm = (float) (hm * 1.5);
            vm = (float) (vm * 1.5);
        }
        float vm_0_5 = vm / 2;
        float vm_1_5 = vm * 1.5f;
        getCustomPiecePlacePositions().add(new PointF(-30, 0));
        getCustomPiecePlacePositions().add(new PointF(0, 0));
        getCustomPiecePlacePositions().add(new PointF(+30, 0));
        getCustomPiecePlacePositions().add(new PointF(0, 0));
        getCustomPiecePlacePositions().add(new PointF(0, 0));
        getCustomPiecePlacePositions().add(new PointF(0, 0));
        getCustomPiecePlacePositions().add(new PointF(0, 0));
        getCustomPiecePlacePositions().add(new PointF(0, 0));
        getCustomPiecePlacePositions().add(new PointF(0, 0));
        getCustomPiecePlacePositions().add(new PointF(0, 0));
        getCustomPiecePlacePositions().add(new PointF(0, 0));
        getCustomPiecePlacePositions().add(new PointF(0, 0));


        getCustomButtonPlacePositions().add(new PointF(-w - hm, -h_1_5 - vm_1_5));
        getCustomButtonPlacePositions().add(new PointF(0, -h_1_5 - vm_1_5));
        getCustomButtonPlacePositions().add(new PointF(+w + hm, -h_1_5 - vm_1_5));
        getCustomButtonPlacePositions().add(new PointF(-w - hm, -h_0_5 - vm_0_5));
        getCustomButtonPlacePositions().add(new PointF(0, -h_0_5 - vm_0_5));
        getCustomButtonPlacePositions().add(new PointF(+w + hm, -h_0_5 - vm_0_5));
        getCustomButtonPlacePositions().add(new PointF(-w - hm, +h_0_5 + vm_0_5));
        getCustomButtonPlacePositions().add(new PointF(0, +h_0_5 + vm_0_5));
        getCustomButtonPlacePositions().add(new PointF(+w + hm, +h_0_5 + vm_0_5));
        getCustomButtonPlacePositions().add(new PointF(-w - hm, +h_1_5 + vm_1_5));
        getCustomButtonPlacePositions().add(new PointF(0, +h_1_5 + vm_1_5));
        getCustomButtonPlacePositions().add(new PointF(+w + hm, +h_1_5 + vm_1_5));


    }

    TextOutsideCircleButton.Builder getTextOutsideCircleButtonBuilderWithDifferentPieceColor(String name, int tag, int id,String color) {

        return new TextOutsideCircleButton.Builder()
                .normalImageRes(id)
                .normalText(name)
                .textSize(getResources().getBoolean(R.bool.isTablet)? 14 : 13)
                .rotateText(true)
                .normalColor(Color.parseColor(color))
                .listener(index -> {

                    if (tag == 9)
                    {
                        new Handler().postDelayed(() -> {
                            context.startActivity(new Intent(context, MainContractFragment.class).putExtra("Type", tag));
                        }, 850);
                        return;
                    }
                    if (tag == 10)
                    {
                        new Handler().postDelayed(() -> {
                            context.startActivity(new Intent(context, MainProfileFragment.class).putExtra("Type", tag));
                        }, 850);
                        return;
                    }
                    if (tag == 0) return;
                    new Handler().postDelayed(() -> {
                        context.startActivity(new Intent(context, com.eligasht.reservation.views.activities.main.MainActivity.class).putExtra("Type", tag));
                    }, 850);

                })
                .typeface(SingletonContext.getInstance().getTypeface())
                .pieceColor(Color.WHITE);
    }
}
