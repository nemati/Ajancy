package com.eligasht.reservation.views.activities.menu.dashbord_fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.eligasht.R;
import com.eligasht.reservation.views.activities.menu.MenuActivity;
import com.eligasht.reservation.views.activities.menu.MenuModel;

import java.util.ArrayList;
import java.util.List;

import mehdi.sakout.fancybuttons.FancyButton;

public class DashbordFragment extends Fragment implements View.OnClickListener {


    public LinearLayout linear_report;
    private View v;


    private List<MenuModel> countries;
    String[] MenuModelArray = {"report", "seller", "contract", "accounting", "checklist", "stroke"
            , "hotel_menu", "group", "tour", "train", "insurance"};

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (v != null)
            return v;
        v = inflater.inflate(R.layout.activity_menu_dashbord, container, false);
        FancyButton iconDashbord = v.findViewById(R.id.iconDashbord);
        iconDashbord.setOnClickListener(this);
        iconDashbord.setCustomTextFont("fonts/fontastic.ttf");
        iconDashbord.setText(getString(R.string.icon_dashbord_one));

        FancyButton iconMenu = v.findViewById(R.id.iconMenu);
        iconMenu.setOnClickListener(this);
        iconMenu.setCustomTextFont("fonts/fontastic.ttf");
        iconMenu.setText(getString(R.string.icon_menu_one));

        ImageView imageView = v.findViewById(R.id.menu_mail);

        FancyButton userIcon = v.findViewById(R.id.userIcon);
        userIcon.setOnClickListener(this);
        userIcon.setCustomTextFont("fonts/fontastic.ttf");
        userIcon.setText(getString(R.string.icon_user_one));




        /*Grid*/
     /*   RecyclerView mRecyclerView = v.findViewById(R.id.list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());


        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen._1dp_margin);
        mRecyclerView.addItemDecoration(new SpacesItemDecoration(spacingInPixels));


        MenuAdapter mAdapter = new MenuAdapter(getCountries(), R.layout.row_menu_main, getContext());

        mRecyclerView.setLayoutManager(new RtlGrid(getContext(), 3));
        mRecyclerView.setAdapter(mAdapter);*/
        return v;
    }


    public List<MenuModel> getCountries() {
        List<String> l2 = new ArrayList<>();
        l2.add("جستجوی پرواز");
        l2.add("جستجوی تور");
        l2.add("جستجوی پکیج");

        l2.add("جستجوی هتل");
        l2.add("جستجوی بیمه");
        l2.add("قراردادها");

        l2.add("فروش خدمات");
        l2.add("گزارشات");
        l2.add("اصلاحیه ها");

        l2.add("حسابداری");
        l2.add("قطار خارجی");
        List<Integer> intImg = new ArrayList<>();
        intImg.add(R.string.icon_menu_search_flight);
        intImg.add(R.string.icon_menu_search_tour);
        intImg.add(R.string.icon_menu_search_package);

        intImg.add(R.string.icon_search_hotel);
        intImg.add(R.string.icon_search_insur);
        intImg.add(R.string.icon_contract);

        intImg.add(R.string.icon_saller_service);
        intImg.add(R.string.icon_report);
        intImg.add(R.string.icon_check_list);

        intImg.add(R.string.icon_accounting);
        intImg.add(R.string.icon_train);


        if (countries == null) {
            countries = new ArrayList<MenuModel>();

            int i = 0;
            for (String MenuModelName : MenuModelArray) {
                MenuModel MenuModel = new MenuModel();
                MenuModel.name = l2.get(i);
                MenuModel.description = "";
                MenuModel.imageName = getString(intImg.get(i));
                countries.add(MenuModel);
                i++;
            }

        }

        return countries;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.linear_report:
                YoYo.with(Techniques.Bounce)
                        .duration(700)
                        .repeat(5)
                        .playOn(v.findViewById(R.id.linear_report));
                break;
            case R.id.iconMenu:
                YoYo.with(Techniques.Bounce)
                        .duration(700)
                        .repeat(1)
                        .playOn(v.findViewById(R.id.iconMenu));

                try {
                    MenuActivity fragment2 = new MenuActivity();
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                    fragmentTransaction.commit();
                    //  getSupportFragmentManager().beginTransaction().replace(R.id.frag_container,  new DashbordFragment()).disallowAddToBackStack().commitAllowingStateLoss();
                } catch (Exception e) {
                }
                break;


        }
    }
}
