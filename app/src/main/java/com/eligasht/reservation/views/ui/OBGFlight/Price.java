package com.eligasht.reservation.views.ui.OBGFlight;

public class Price {
    private Float Amount ;

    private String CurrencyCode ;

	public String getCurrencyCode() {
		return CurrencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		CurrencyCode = currencyCode;
	}

	public Float getAmount() {
		return Amount;
	}

	public void setAmount(Float amount) {
		Amount = amount;
	}

}
