package com.eligasht.reservation.views.ui.dialog.flight;

/**
 * Created by Mahsa.azizi on 1/10/2018.
 */

public class StringAirLines {
    String string;

    public StringAirLines(String string) {
        this.string = string;
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }
}
