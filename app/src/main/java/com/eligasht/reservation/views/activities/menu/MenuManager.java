package com.eligasht.reservation.views.activities.menu;


import java.util.ArrayList;
import java.util.List;

public class MenuManager {
    private static String[] MenuModelArray = {"report", "seller", "contract", "accounting", "checklist", "stroke"
            , "hotel_menu", "group", "tour", "train", "insurance"};
   private static String[] MenuNameArray = {"report", "seller", "contract", "accounting", "checklist", "stroke"
            , "hotel_menu", "group", "tour", "train", "insurance"};



    private static String loremIpsum = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut " +
            "labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea " +
            "commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. " +
            "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";


    private static MenuManager mInstance;
    private List<MenuModel> countries;
    public static MenuManager getInstance() {
        if (mInstance == null) {
            mInstance = new MenuManager();
        }

        return mInstance;
    }

    public List<MenuModel> getCountries() {
        if (countries == null) {
            countries = new ArrayList<MenuModel>();
            for (int i = 0; i <MenuModelArray.length+1 ; i++) {

            }
            int i=-1;
            for (String MenuModelName : MenuModelArray) {
                MenuModel MenuModel = new MenuModel();
                MenuModel.name = MenuModelName;
                MenuModel.description = loremIpsum;
                MenuModel.imageName = MenuModelName.replaceAll("\\s+","").toLowerCase();
                countries.add(MenuModel);
            }

        }

        return  countries;
    }

}
