package com.eligasht.reservation.views.activities.menu;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;


import com.eligasht.R;
import com.eligasht.reservation.tools.GlideApp;
import com.eligasht.reservation.views.activities.new_login.StatusBarUtil;

import java.util.Calendar;
import java.util.Date;

import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.HorizontalCalendarListener;


public class DashboardFragment extends Fragment
{

	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v=inflater.inflate(R.layout.fragment_dashboard, container, false);
		ImageView imageView = v.findViewById(R.id.imgDashbord);
		GlideApp.with(getContext()).load(R.drawable.dashbord).into(imageView);
		return v;
	}


}
