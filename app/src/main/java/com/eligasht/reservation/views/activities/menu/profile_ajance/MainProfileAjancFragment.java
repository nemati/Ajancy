package com.eligasht.reservation.views.activities.menu.profile_ajance;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eligasht.R;
import com.eligasht.reservation.views.activities.menu.profile_ajance.Pager;
import com.eligasht.reservation.views.ui.SingletonContext;
import com.kazy.fontdrawable.FontDrawable;

import java.util.ArrayList;

import devlight.io.library.ntb.NavigationTabBar;


/**
 * Created by Ahmad.nemati on 7/14/2018.
 */
public class MainProfileAjancFragment extends Fragment implements TabLayout.OnTabSelectedListener {


    private NavigationTabBar tabLayout;

    //This is our viewPager
    private ViewPager viewPager;

    View v;
    static final String CUSTOM_FONT_PATH = "fonts/fontastic.ttf";


    String font_cod_manage_user, font_cod_ajanc, font_cod_attach = "";

    @ColorInt
    static final int MATERIAL_BLUE = 0xff00a8f7;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_profile, container, false);
        font_cod_manage_user = getString(R.string.user_outline) + "";
        font_cod_ajanc = getString(R.string.ajance);
        font_cod_attach = getString(R.string.peyvast);



        initUI();
        return v;
    }

    private void initUI() {
        final ViewPager viewPager = v.findViewById(R.id.view_pager);
        Pager pager = new Pager(getChildFragmentManager(), 3);
        viewPager.setAdapter(pager);

        final String[] colors = getResources().getStringArray(R.array.medical_black_white);

        final NavigationTabBar navigationTabBar = v.findViewById(R.id.tab_layout);
        navigationTabBar.setTypeface(SingletonContext.getInstance().getTypeface());
        final ArrayList<NavigationTabBar.Model> models = new ArrayList<>();

        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_content_paste_black_24dp),
                        Color.parseColor(colors[0]))
                        .selectedIcon( getResources().getDrawable(R.drawable.ic_content_paste_black_24dp))
                        .title("مدارک پیوست")
                        .badgeTitle("NTB")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_group_black_24dp),
                        Color.parseColor(colors[1]))
                        .selectedIcon(getResources().getDrawable(R.drawable.ic_group_black_24dp))
                        .title("مدیریت کاربران")
                        .badgeTitle("with")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(getResources().getDrawable(R.drawable.ic_location_searching_black_24dp),
                        // getResources().getDrawable(R.drawable._1star),
                        Color.parseColor(colors[2]))
                        .selectedIcon(getResources().getDrawable(R.drawable.ic_location_searching_black_24dp))
                        .title("آژانس")
                        .badgeTitle("state")
                        .build()
        );


        navigationTabBar.setModels(models);
        navigationTabBar.setViewPager(viewPager, 2);

        navigationTabBar.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(final int position) {
                navigationTabBar.getModels().get(position).hideBadge();
            }

            @Override
            public void onPageScrollStateChanged(final int state) {

            }
        });

        navigationTabBar.postDelayed(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < navigationTabBar.getModels().size(); i++) {
                    final NavigationTabBar.Model model = navigationTabBar.getModels().get(i);
                    navigationTabBar.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            model.showBadge();
                        }
                    }, i * 100);
                }
            }
        }, 500);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}




