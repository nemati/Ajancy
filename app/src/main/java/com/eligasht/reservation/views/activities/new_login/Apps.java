package com.eligasht.reservation.views.activities.new_login;

import android.app.Application;

public class Apps extends Application
{
	@Override
	public void onCreate() {
		FontsOverride.setDefaultFont(this, "sans","fonts/apercu_regular.otf");
		FontsOverride.setDefaultFont(this, "SERIF","fonts/iran_sans_normal.ttf");
		super.onCreate();
	}
}
