package com.eligasht.reservation.views.activities.menu;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.eligasht.R;
import com.eligasht.reservation.tools.GlideApp;
import com.minibugdev.drawablebadge.BadgePosition;
import com.minibugdev.drawablebadge.DrawableBadge;

public class MenuActivity extends Fragment {


    private View v;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (v != null)
            return v;


        v = inflater.inflate(R.layout.activity_menu, container, false);
        ImageView cont = v.findViewById(R.id.container);
        GlideApp.with(getContext()).load(R.drawable.dashbord).into(cont);

        ImageView imageView = v.findViewById(R.id.menu_mail);

        DrawableBadge drawableBadge = new DrawableBadge.Builder(getContext())
                .drawableResId(R.drawable.ic_mail_outline_black_24dp)
                .badgeColor(R.color.main)
                .badgeSize(R.dimen._4dp_normal_margin)
                .badgePosition(BadgePosition.TOP_RIGHT)
                .textColor(R.color.white)
                .showBorder(false)
                .maximumCounter(2)
                .build();
        imageView.setImageDrawable(drawableBadge.get(2));


        return v;

    }


}
