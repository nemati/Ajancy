package com.eligasht.reservation.views.activities.menu;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eligasht.R;
import com.eligasht.reservation.views.activities.menu.adapter.FragmentProfileAdapter;
import com.eligasht.reservation.views.activities.new_login.StatusBarUtil;

/**
 * Created by Ahmad.nemati on 7/21/2018.
 */
public class MainProfileFragment extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_main_profile);
        StatusBarUtil.immersive(this);
        tabLayout = findViewById(R.id.tab_layout);
        viewPager = findViewById(R.id.view_pager);
        FragmentProfileAdapter adapter = new FragmentProfileAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }
}
