package com.eligasht.reservation.views.activities.menu;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.util.AttributeSet;

/**
 * Created by Ahmad.nemati on 7/11/2018.
 */
public class RtlGrid extends GridLayoutManager {
    public RtlGrid(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public RtlGrid(Context context, int spanCount) {
        super(context, spanCount);
    }

    public RtlGrid(Context context, int spanCount, int orientation, boolean reverseLayout) {
        super(context, spanCount, orientation, reverseLayout);
    }

    @Override
    protected boolean isLayoutRTL() {
        return true;
    }
}
