package com.eligasht.reservation.views.activities.menu.profile_me;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eligasht.R;

/**
 * Created by Ahmad.nemati on 7/22/2018.
 */
public class MyProfileFragment extends Fragment {
    private View v;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (v != null)
            return v;

        v = inflater.inflate(R.layout.fragment_profile2, container, false);
        return v;
    }
}
