package com.eligasht.reservation.views.activities.menu.profile_ajance;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.eligasht.R;
import com.eligasht.reservation.tools.GlideApp;

public class ManagerUserTab extends Fragment {

    private View v;


    public static ManagerUserTab instance() {
        ManagerUserTab fragment = new ManagerUserTab();
        return fragment;
    }

    //Overriden method onCreateView
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (v != null)
            return v;


        //Returning the layout file after inflating
        //Change R.layout.tab1 in you classes
        v = inflater.inflate(R.layout.search_user_fragment, container, false);
        GlideApp.with(getContext()).load(R.drawable.back45).into((ImageView) v.findViewById(R.id.img1));
        GlideApp.with(getContext()).load(R.drawable.back45).into((ImageView) v.findViewById(R.id.img2));
        GlideApp.with(getContext()).load(R.drawable.back45).into((ImageView) v.findViewById(R.id.img3));
        GlideApp.with(getContext()).load(R.drawable.back45).into((ImageView) v.findViewById(R.id.img4));
        GlideApp.with(getContext()).load(R.drawable.back45).into((ImageView) v.findViewById(R.id.img5));
        GlideApp.with(getContext()).load(R.drawable.back45).into((ImageView) v.findViewById(R.id.img7));
        GlideApp.with(getContext()).load(R.drawable.back45).into((ImageView) v.findViewById(R.id.img8));
        GlideApp.with(getContext()).load(R.drawable.back45).into((ImageView) v.findViewById(R.id.img9));

        return v;
    }
}