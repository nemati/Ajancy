package com.eligasht.reservation.views.activities.menu.contractMenu;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.eligasht.R;
import com.eligasht.reservation.base.BaseActivity;
import com.eligasht.reservation.views.activities.menu.adapter.FragmentContractAdapter;

import com.eligasht.reservation.views.activities.menu.profile_ajance.Pager;
import com.eligasht.reservation.views.activities.new_login.StatusBarUtil;
import com.eligasht.reservation.views.ui.SingletonContext;

import java.util.ArrayList;

import devlight.io.library.ntb.NavigationTabBar;

public class MainContractFragment extends BaseActivity implements TabLayout.OnTabSelectedListener {

    String font_cod_manage_user, font_cod_ajanc, font_cod_attach = "";

    ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_main_contract);
        StatusBarUtil.immersive(this);
        viewPager =  findViewById(R.id.view_pager);
        font_cod_manage_user = getString(R.string.user_outline) + "";
        font_cod_ajanc = getString(R.string.ajance);
        font_cod_attach = getString(R.string.peyvast);

     initUI();

    }

    private void initUI() {
        final ViewPager viewPager = findViewById(R.id.view_pager);
        FragmentContractAdapter pager = new FragmentContractAdapter(getSupportFragmentManager(), 2);
        viewPager.setAdapter(pager);

        final String[] colors = getResources().getStringArray(R.array.medical_black_white);

        final NavigationTabBar navigationTabBar =(NavigationTabBar) findViewById(R.id.tab_layout);
        navigationTabBar.setTypeface(SingletonContext.getInstance().getTypeface());
        final ArrayList<NavigationTabBar.Model> models = new ArrayList<>();

        models.add(new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_find_in_page_black_24dp),
                        Color.parseColor(colors[1]))
                        .selectedIcon( getResources().getDrawable(R.drawable.ic_find_in_page_black_24dp))
                        .title("جستجوی قراردادها")
                        .badgeTitle("NTB")
                        .build());
        models.add(new NavigationTabBar.Model.Builder(getResources().getDrawable(R.drawable.ic_content_paste_black_24dp),Color.parseColor(colors[0]))
                        .selectedIcon(getResources().getDrawable(R.drawable.ic_content_paste_black_24dp))
                        .title("مشاهده 10 قرارداد آخر")
                        .badgeTitle("with")
                        .build());



        navigationTabBar.setModels(models);
        navigationTabBar.setViewPager(viewPager, 2);

        navigationTabBar.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(final int position) {
                navigationTabBar.getModels().get(position).hideBadge();
            }

            @Override
            public void onPageScrollStateChanged(final int state) {

            }
        });

        navigationTabBar.postDelayed(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < navigationTabBar.getModels().size(); i++) {
                    final NavigationTabBar.Model model = navigationTabBar.getModels().get(i);
                    navigationTabBar.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            model.showBadge();
                        }
                    }, i * 100);
                }
            }
        }, 500);
    }

  @Override
    public void onTabSelected(TabLayout.Tab tab) {

        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public boolean needTerminate() {
        return false;
    }
}




