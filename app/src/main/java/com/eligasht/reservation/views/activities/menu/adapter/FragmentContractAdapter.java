package com.eligasht.reservation.views.activities.menu.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.eligasht.reservation.views.activities.menu.contractMenu.LastContractFragment;
import com.eligasht.reservation.views.activities.menu.contractMenu.SearchContractFragment;
import com.eligasht.reservation.views.activities.menu.profile_ajance.AccountAjanceTab;
import com.eligasht.reservation.views.activities.menu.profile_ajance.AttachTab;
import com.eligasht.reservation.views.activities.menu.profile_ajance.MainProfileAjancFragment;
import com.eligasht.reservation.views.activities.menu.profile_ajance.ManagerUserTab;
import com.eligasht.reservation.views.activities.menu.profile_me.MyProfileFragment;

/**
 * Created by Mahsa.azizi on 7/24/2018.
 */
public class FragmentContractAdapter  extends FragmentPagerAdapter {

    //integer to count number of tabs
    int tabCount;

    private LastContractFragment lastContractFragment;
    private SearchContractFragment searchContractFragment;
    private AttachTab attach;

    //Constructor to the class
    public FragmentContractAdapter(FragmentManager fm, int tabCount) {
        super(fm);
        //Initializing tab count
        this.tabCount= tabCount;



    }

    //Overriding method getItem
    @Override
    public Fragment getItem(int position) {
        //Returning the current tabs

        switch (position) {

            case 1:
                searchContractFragment = SearchContractFragment.instance();
                return searchContractFragment;
            case 0:
                lastContractFragment = LastContractFragment.instance();
                return lastContractFragment;

            default:
                searchContractFragment = SearchContractFragment.instance();
                return searchContractFragment;
        }
    }

    //Overriden method getCount to get the number of tabs
    @Override
    public int getCount() {
        return tabCount;
    }
}