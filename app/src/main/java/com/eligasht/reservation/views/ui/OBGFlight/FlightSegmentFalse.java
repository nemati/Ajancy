package com.eligasht.reservation.views.ui.OBGFlight;

import net.sourceforge.jtds.jdbc.DateTime;

public class FlightSegmentFalse {
    private String AirlineCode ;

    private int AirlineID ;

    private String AirlineNameEn ;

    private String AirlineNameFa ;

    private String AirplaneName ;

    private String ArrivalAirportCode ;

    private String ArrivalAirportNameEn ;

    private String ArrivalAirportNameFa ;

    private String ArrivalCityCode ;

    private String ArrivalCityNameEn ;

    private String ArrivalCityNameFa ;

    private String ArrivalCountryNameEn ;

    private String ArrivalCountryNameFa ;

    private DateTime ArrivalDate ;

    private String ArrivalDateShamsi ;

    private String CabinClassCode ;

    private String CabinClassName ;

    private String CabinClassNameFa ;

    private String DepartureAirportCode ;

    private String DepartureAirportNameEn ;

    private String DepartureAirportNameFa ;

    private String DepartureCityCode ;
	private  String weight;
	public String getPieces() {
		return Pieces;
	}

	public void setPieces(String pieces) {
		Pieces = pieces;
	}

	private  String Pieces;
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
    public String getAirlineCode() {
		return AirlineCode;
	}

	public void setAirlineCode(String airlineCode) {
		AirlineCode = airlineCode;
	}

	public int getAirlineID() {
		return AirlineID;
	}

	public void setAirlineID(int airlineID) {
		AirlineID = airlineID;
	}

	public String getAirlineNameEn() {
		return AirlineNameEn;
	}

	public void setAirlineNameEn(String airlineNameEn) {
		AirlineNameEn = airlineNameEn;
	}

	public String getAirlineNameFa() {
		return AirlineNameFa;
	}

	public void setAirlineNameFa(String airlineNameFa) {
		AirlineNameFa = airlineNameFa;
	}

	public String getAirplaneName() {
		return AirplaneName;
	}

	public void setAirplaneName(String airplaneName) {
		AirplaneName = airplaneName;
	}

	public String getArrivalAirportCode() {
		return ArrivalAirportCode;
	}

	public void setArrivalAirportCode(String arrivalAirportCode) {
		ArrivalAirportCode = arrivalAirportCode;
	}

	public String getArrivalAirportNameEn() {
		return ArrivalAirportNameEn;
	}

	public void setArrivalAirportNameEn(String arrivalAirportNameEn) {
		ArrivalAirportNameEn = arrivalAirportNameEn;
	}

	public String getArrivalAirportNameFa() {
		return ArrivalAirportNameFa;
	}

	public void setArrivalAirportNameFa(String arrivalAirportNameFa) {
		ArrivalAirportNameFa = arrivalAirportNameFa;
	}

	public String getArrivalCityCode() {
		return ArrivalCityCode;
	}

	public void setArrivalCityCode(String arrivalCityCode) {
		ArrivalCityCode = arrivalCityCode;
	}

	public String getArrivalCityNameEn() {
		return ArrivalCityNameEn;
	}

	public void setArrivalCityNameEn(String arrivalCityNameEn) {
		ArrivalCityNameEn = arrivalCityNameEn;
	}

	public String getArrivalCityNameFa() {
		return ArrivalCityNameFa;
	}

	public void setArrivalCityNameFa(String arrivalCityNameFa) {
		ArrivalCityNameFa = arrivalCityNameFa;
	}

	public String getArrivalCountryNameEn() {
		return ArrivalCountryNameEn;
	}

	public void setArrivalCountryNameEn(String arrivalCountryNameEn) {
		ArrivalCountryNameEn = arrivalCountryNameEn;
	}

	public String getArrivalCountryNameFa() {
		return ArrivalCountryNameFa;
	}

	public void setArrivalCountryNameFa(String arrivalCountryNameFa) {
		ArrivalCountryNameFa = arrivalCountryNameFa;
	}

	public DateTime getArrivalDate() {
		return ArrivalDate;
	}

	public void setArrivalDate(DateTime arrivalDate) {
		ArrivalDate = arrivalDate;
	}

	public String getArrivalDateShamsi() {
		return ArrivalDateShamsi;
	}

	public void setArrivalDateShamsi(String arrivalDateShamsi) {
		ArrivalDateShamsi = arrivalDateShamsi;
	}

	public String getCabinClassCode() {
		return CabinClassCode;
	}

	public void setCabinClassCode(String cabinClassCode) {
		CabinClassCode = cabinClassCode;
	}

	public String getCabinClassName() {
		return CabinClassName;
	}

	public void setCabinClassName(String cabinClassName) {
		CabinClassName = cabinClassName;
	}

	public String getCabinClassNameFa() {
		return CabinClassNameFa;
	}

	public void setCabinClassNameFa(String cabinClassNameFa) {
		CabinClassNameFa = cabinClassNameFa;
	}

	public String getDepartureAirportCode() {
		return DepartureAirportCode;
	}

	public void setDepartureAirportCode(String departureAirportCode) {
		DepartureAirportCode = departureAirportCode;
	}

	public String getDepartureAirportNameEn() {
		return DepartureAirportNameEn;
	}

	public void setDepartureAirportNameEn(String departureAirportNameEn) {
		DepartureAirportNameEn = departureAirportNameEn;
	}

	public String getDepartureAirportNameFa() {
		return DepartureAirportNameFa;
	}

	public void setDepartureAirportNameFa(String departureAirportNameFa) {
		DepartureAirportNameFa = departureAirportNameFa;
	}

	public String getDepartureCityCode() {
		return DepartureCityCode;
	}

	public void setDepartureCityCode(String departureCityCode) {
		DepartureCityCode = departureCityCode;
	}

	public String getDepartureCityNameEn() {
		return DepartureCityNameEn;
	}

	public void setDepartureCityNameEn(String departureCityNameEn) {
		DepartureCityNameEn = departureCityNameEn;
	}

	public String getDepartureCityNameFa() {
		return DepartureCityNameFa;
	}

	public void setDepartureCityNameFa(String departureCityNameFa) {
		DepartureCityNameFa = departureCityNameFa;
	}

	public String getDepartureCountryNameEn() {
		return DepartureCountryNameEn;
	}

	public void setDepartureCountryNameEn(String departureCountryNameEn) {
		DepartureCountryNameEn = departureCountryNameEn;
	}

	public String getDepartureCountryNameFa() {
		return DepartureCountryNameFa;
	}

	public void setDepartureCountryNameFa(String departureCountryNameFa) {
		DepartureCountryNameFa = departureCountryNameFa;
	}

	public DateTime getDepartureDate() {
		return DepartureDate;
	}

	public void setDepartureDate(DateTime departureDate) {
		DepartureDate = departureDate;
	}

	public String getDepartureDateShamsi() {
		return DepartureDateShamsi;
	}

	public void setDepartureDateShamsi(String departureDateShamsi) {
		DepartureDateShamsi = departureDateShamsi;
	}

	public String getFlightArrivalTime() {
		return FlightArrivalTime;
	}

	public void setFlightArrivalTime(String flightArrivalTime) {
		FlightArrivalTime = flightArrivalTime;
	}

	public String getFlightNumber() {
		return FlightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		FlightNumber = flightNumber;
	}

	public String getFlightTime() {
		return FlightTime;
	}

	public void setFlightTime(String flightTime) {
		FlightTime = flightTime;
	}

	public String getFltDateDayOfWeek() {
		return FltDateDayOfWeek;
	}

	public void setFltDateDayOfWeek(String fltDateDayOfWeek) {
		FltDateDayOfWeek = fltDateDayOfWeek;
	}

	public String getFltDurationH() {
		return FltDurationH;
	}

	public void setFltDurationH(String fltDurationH) {
		FltDurationH = fltDurationH;
	}

	public String getFltDurationM() {
		return FltDurationM;
	}

	public void setFltDurationM(String fltDurationM) {
		FltDurationM = fltDurationM;
	}

	public boolean isIsDepartureSegment() {
		return IsDepartureSegment;
	}

	public void setIsDepartureSegment(boolean isDepartureSegment) {
		IsDepartureSegment = isDepartureSegment;
	}

	private String DepartureCityNameEn ;

    private String DepartureCityNameFa ;

    private String DepartureCountryNameEn ;

    private String DepartureCountryNameFa ;

    private DateTime DepartureDate ;

    private String DepartureDateShamsi ;

    private String FlightArrivalTime ;

    private String FlightNumber ;

    private String FlightTime ;

    private String FltDateDayOfWeek ;

    private String FltDurationH ;

    private String FltDurationM ;

    private boolean IsDepartureSegment ;

}
