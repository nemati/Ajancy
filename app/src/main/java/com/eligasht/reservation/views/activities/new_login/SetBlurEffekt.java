package com.eligasht.reservation.views.activities.new_login;

/**
 * Created by Ahmad.nemati on 7/25/2018.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.util.Log;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;


/**
 * Created by erkan_i7 on 22.09.2017.
 *
 */

public class SetBlurEffekt implements TextureView.SurfaceTextureListener {

    private RenderScript mRS;
    private ScriptIntrinsicBlur scriptIntrinsicBlur;
    private Allocation allocOriginalScreenshot, allocBlurred;

    private TextureView textureViewBlurred;
    private Context mContext;


    public SetBlurEffekt(Context cnt) {
        mContext = cnt;
        // Initialize RenderScript
        mRS = RenderScript.create(mContext);
        scriptIntrinsicBlur = ScriptIntrinsicBlur.create(mRS, Element.RGBA_8888(mRS));
        scriptIntrinsicBlur.setRadius(5);
    }

    private Bitmap getViewScreenshot(View v) {
        v.setDrawingCacheEnabled(true);
        Bitmap b = Bitmap.createBitmap(v.getDrawingCache());
        v.setDrawingCacheEnabled(false);

        return b;
    }

    public void blurView(View v) {
        // First of all, take a screen of the current view
        Bitmap viewScreenshot = getViewScreenshot(v);

        // Defines allocations where to store the screenshot and the temporary blurred image
        if (allocOriginalScreenshot != null && (allocOriginalScreenshot.getType().getX() != viewScreenshot.getWidth() ||
                allocOriginalScreenshot.getType().getY() != viewScreenshot.getHeight())) {

            // Current allocations have wrong sizes!
            allocOriginalScreenshot.destroy();
            allocBlurred.destroy();

            textureViewBlurred = null;
            allocOriginalScreenshot = null;
            allocBlurred = null;
        }

        if (allocOriginalScreenshot == null) {
            allocOriginalScreenshot = Allocation.createFromBitmap(mRS, viewScreenshot);
            // Creates an allocation where to store the blur results
            allocBlurred = Allocation.createTyped(mRS, allocOriginalScreenshot.getType(), Allocation.USAGE_SCRIPT | Allocation.USAGE_IO_OUTPUT);

            // Then, replace the current view with a TextureView.
            // This lets RenderScript display the blurred screenshot with ease, without killing
            // the memory (in case of large screenshots)
            textureViewBlurred = new TextureView(mContext);
            textureViewBlurred.setOpaque(false);
            textureViewBlurred.setSurfaceTextureListener(this);

        } else {
            // Just copy the new view screenshot
            allocOriginalScreenshot.copyFrom(viewScreenshot);
        }

        replaceView(v, textureViewBlurred);

    }

    public void unblurView(View v) {
        restoreView(v);
    }

    /*
     Restores the original view. Checks if there is a valid view inside the tag field, and restores
     it.
      */
    void restoreView(View v) {
        try {
            View otherView = (View) v.getTag();

            if (otherView != null && otherView.getParent() != null) {
                replaceView(otherView, v);
            } else if (v != null && v.getParent() != null) {
                replaceView(v, otherView);
            }
        }
        catch (Exception e)
        {

        }

    }

    void replaceView(View originalView, View newView) {
        originalView.setTag(newView);

        newView.setLayoutParams(new FrameLayout.LayoutParams(originalView.getLayoutParams()));

        ViewGroup parent = (ViewGroup) originalView.getParent();
        int index = parent.indexOfChild(originalView);
        parent.removeView(originalView);

        parent.addView(newView, index);
    }

    // Performs the actual blur calculation
    void executeBlur() {
        Log.d("BLUR ", "Executing blur");

        scriptIntrinsicBlur.setInput(allocOriginalScreenshot);
        scriptIntrinsicBlur.forEach(allocBlurred);

        allocBlurred.ioSend();
    }



    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i1) {
        // Once the surface is ready, execute the blur
        allocBlurred.setSurface(new Surface(surfaceTexture));

        executeBlur();
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i1) {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {

    }
}
