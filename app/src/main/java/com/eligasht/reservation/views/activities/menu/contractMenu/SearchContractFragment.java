package com.eligasht.reservation.views.activities.menu.contractMenu;

import android.animation.Animator;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.eligasht.R;
import com.eligasht.reservation.models.model.ModelRowCountRoom;
import com.eligasht.reservation.tools.Prefs;
import com.eligasht.reservation.views.adapters.HotelCountRoomAdapter;
import com.eligasht.reservation.views.picker.global.enums.TypeUsageOfCalendar;
import com.eligasht.reservation.views.picker.global.listeners.ICallbackCalendarDialog;
import com.eligasht.reservation.views.picker.global.model.CustomDate;
import com.eligasht.reservation.views.picker.global.model.SingletonDate;
import com.eligasht.reservation.views.picker.utils.CalendarDialog;
import com.eligasht.reservation.views.ui.dialog.app.CountTimeAlert;

import java.util.List;


public class SearchContractFragment extends Fragment implements View.OnClickListener,
        CountTimeAlert.TimerDialogListener
        , ICallbackCalendarDialog {
    private View rootView;
    CalendarDialog calendarDialog,calendarDialogContract;
    public TextView  tarikh_be ,tarikh_be_contract ;
    public List<ModelRowCountRoom> data,data_contract;
    private LottieAnimationView lottieCheckin, lottieCheckout,lottieCheckin_contract, lottieCheckout_contract;
    TextView tvRaft, tvBargasht,tvRaft_contract, tvBargasht_contract;
    String raft, bargasht,raft_contract, bargasht_contract;
    LinearLayout  llRaft, llBargasht,llRaft_contract, llBargasht_contract;
    private int flagContract;

    public static SearchContractFragment instance()  {
        SearchContractFragment fragment = new SearchContractFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView != null)
            return rootView;

        rootView = inflater.inflate(R.layout.fragment_search_contract, container, false);

        //Date picker Travel
        untilDatePickerTravel();

        //Date picker Contract
        untilDatePickerContract();

        return rootView;
    }

    private void untilDatePickerTravel() {
        calendarDialog = new CalendarDialog();
        SingletonDate.getInstance().checkConflictDate();
        if (CustomDate.compareTwoDays(SingletonDate.getInstance().getStartDate().getCalendar(), SingletonDate.getInstance().getEndDate().getCalendar()) == 0) {
            SingletonDate.getInstance().getEndDate().addOneDay();
        }
        tarikh_be = rootView.findViewById(R.id.tarikh_be);
        lottieCheckin = rootView.findViewById(R.id.lottie_checkin);
        lottieCheckout = rootView.findViewById(R.id.lottie_checkout);
        lottieCheckin.setSpeed(2f);
        lottieCheckout.setSpeed(2f);
        tarikh_be.setOnClickListener(this);

        tvRaft = rootView.findViewById(R.id.tvRaft);
        tvBargasht = rootView.findViewById(R.id.tvBargasht);

        llRaft = (LinearLayout)rootView.findViewById(R.id.llRaft);
        llBargasht = (LinearLayout)rootView.findViewById(R.id.llBargasht);

        llRaft.setOnClickListener(this);
        llBargasht.setOnClickListener(this);

        tvBargasht.setText(SingletonDate.getInstance().getEndDate().getDescription());
        bargasht = SingletonDate.getInstance().getEndDate().getFullGeo();
        tvRaft.setText(SingletonDate.getInstance().getStartDate().getDescription());
        raft = SingletonDate.getInstance().getStartDate().getFullGeo();
    }

    private void untilDatePickerContract() {
        calendarDialogContract = new CalendarDialog();

        SingletonDate.getInstance().checkConflictDate();
        if (CustomDate.compareTwoDays(SingletonDate.getInstance().getStartDate().getCalendar(), SingletonDate.getInstance().getEndDate().getCalendar()) == 0) {
            SingletonDate.getInstance().getEndDate().addOneDay();
        }
        tarikh_be_contract = rootView.findViewById(R.id.tarikh_be_contract);
        lottieCheckin_contract = rootView.findViewById(R.id.lottie_checkin_contract);
        lottieCheckout_contract = rootView.findViewById(R.id.lottie_checkout_contract);
        lottieCheckin_contract.setSpeed(2f);
        lottieCheckout_contract.setSpeed(2f);
        tarikh_be_contract.setOnClickListener(this);

        tvRaft_contract = rootView.findViewById(R.id.tvRaft_contract);
        tvBargasht_contract = rootView.findViewById(R.id.tvBargasht_contract);

        llRaft_contract = (LinearLayout)rootView.findViewById(R.id.llRaft_contract);
        llBargasht_contract = (LinearLayout)rootView.findViewById(R.id.llBargasht_contract);

        llRaft_contract.setOnClickListener(this);
        llBargasht_contract.setOnClickListener(this);

        tvBargasht_contract.setText(SingletonDate.getInstance().getEndDate().getDescription());
        bargasht_contract = SingletonDate.getInstance().getEndDate().getFullGeo();
        tvRaft_contract.setText(SingletonDate.getInstance().getStartDate().getDescription());
        raft_contract = SingletonDate.getInstance().getStartDate().getFullGeo();

        calendarDialogContract.setOnDateSelectListener(new ICallbackCalendarDialog() {
            @Override
            public void onDateSelected(CustomDate startDate, CustomDate endDate, boolean isGeo) {
                SingletonDate.getInstance().setReverseDate(startDate, endDate);
                tvRaft_contract.setText(startDate.getDescription());
                tvBargasht_contract.setText(endDate.getDescription());
                initCheckInCheckOutAnimContract();
                raft_contract = startDate.getFullGeo();
                bargasht_contract = endDate.getFullGeo();
            }
        });
    }


    @Override
    public void onClick(View v) {
        Log.e( "onClick: ", v.getId()+"");
       switch (v.getId()){
           //date picker travel
           case R.id.llRaft:
               flagContract=0;
               calendarDialog.create(getActivity(), getContext(), this, SingletonDate.getInstance().getStartDate(), SingletonDate.getInstance().getEndDate(), TypeUsageOfCalendar.HOTEL);
            break;

           case R.id.llBargasht:
               calendarDialog.create(getActivity(), getContext(), this, SingletonDate.getInstance().getStartDate(), SingletonDate.getInstance().getEndDate(), TypeUsageOfCalendar.HOTEL);
               tvRaft.setText(SingletonDate.getInstance().getStartDate().getDescription());
               tvBargasht.setText(SingletonDate.getInstance().getEndDate().getDescription());
               raft = SingletonDate.getInstance().getStartDate().getFullGeo();
               bargasht = SingletonDate.getInstance().getEndDate().getFullGeo();
           break;
           //date picker contract
           case R.id.llRaft_contract:
               calendarDialogContract.create(getActivity(), getContext(), this, SingletonDate.getInstance().getStartDate(), SingletonDate.getInstance().getEndDate(), TypeUsageOfCalendar.HOTEL);
              flagContract=1;
               break;

           case R.id.llBargasht_contract:
               calendarDialogContract.create(getActivity(), getContext(), this, SingletonDate.getInstance().getStartDate(), SingletonDate.getInstance().getEndDate(), TypeUsageOfCalendar.HOTEL);
               tvRaft_contract.setText(SingletonDate.getInstance().getStartDate().getDescription());
               tvBargasht_contract.setText(SingletonDate.getInstance().getEndDate().getDescription());
               raft_contract = SingletonDate.getInstance().getStartDate().getFullGeo();
               bargasht_contract = SingletonDate.getInstance().getEndDate().getFullGeo();
               break;
        }
    }
    private void initCheckInCheckOutAnim() {
        lottieCheckin.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                lottieCheckin.setFrame(0);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        lottieCheckout.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                lottieCheckout.setFrame(0);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        lottieCheckin.playAnimation();
        lottieCheckout.playAnimation();
    }
    private void initCheckInCheckOutAnimContract() {
        lottieCheckin_contract.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                lottieCheckin_contract.setFrame(0);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        lottieCheckout_contract.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                lottieCheckout_contract.setFrame(0);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        lottieCheckin_contract.playAnimation();
        lottieCheckout_contract.playAnimation();
    }


    @Override
    public void onDateSelected(CustomDate startDate, CustomDate endDate, boolean isGeo) {
        if(flagContract==1){
            SingletonDate.getInstance().setReverseDate(startDate, endDate);
            tvRaft_contract.setText(startDate.getDescription());
            tvBargasht_contract.setText(endDate.getDescription());
            initCheckInCheckOutAnimContract();
            raft_contract = startDate.getFullGeo();
            bargasht_contract = endDate.getFullGeo();
        }else {
            SingletonDate.getInstance().setReverseDate(startDate, endDate);
            tvRaft.setText(startDate.getDescription());
            tvBargasht.setText(endDate.getDescription());
            initCheckInCheckOutAnim();
            raft = startDate.getFullGeo();
            bargasht = endDate.getFullGeo();
        }
    }


    @Override
    public void onReturnValue(int type) {

    }
}