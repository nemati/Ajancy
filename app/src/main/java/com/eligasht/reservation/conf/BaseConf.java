package com.eligasht.reservation.conf;

/**
 * Authors:
 * Reza Nejati <reza.n.j.t.i@gmail.com>
 * Copyright © 2017
 */
public class BaseConf {
    private final String TAG = "__" + this.getClass().getSimpleName().toUpperCase().toString();

    public static final String APP_VERSION = "1.0.0";


}
