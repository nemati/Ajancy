package com.eligasht.reservation.tools;

/**
 * Created by Reza.nejati on 2/26/2018.
 */

public interface OnDetectScrollListener {
    void onUpScrolling();

    void onDownScrolling();
    void onFirstVisibleItem();
}
