package com.eligasht.reservation.models.hotel.api.hotelPolicy.response;

/**
 * Created by Reza.nejati on 1/21/2018.
 */

public class GetHotelPolicyResponse {
    public final Object GetHotelPolicyResult;

    public GetHotelPolicyResponse(Object getHotelPolicyResult) {
        GetHotelPolicyResult = getHotelPolicyResult;
    }
}
