package com.eligasht.reservation.models.model.login.response;



public class WebUserChangePasswordRes {

    private EmailContractResult WebUserChangePasswordResult;

    public EmailContractResult getWebUserChangePasswordResult() {
        return WebUserChangePasswordResult;
    }

    public void setWebUserChangePasswordResult(EmailContractResult emailContractResult) {
        WebUserChangePasswordResult = emailContractResult;
    }
}
