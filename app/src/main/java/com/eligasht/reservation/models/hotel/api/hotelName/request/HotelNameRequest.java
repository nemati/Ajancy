package com.eligasht.reservation.models.hotel.api.hotelName.request;

/**
 * Created by Reza.nejati on 2/13/2018.
 */

public class HotelNameRequest {
    public final HotelNameRequestModel request;

    public HotelNameRequest(HotelNameRequestModel request) {
        this.request = request;
    }
}
