package com.eligasht.reservation.models.hotel.api.hotelAvail.response;

/**
 * Created by Reza.nejati on 1/13/2018.
 */

public class HotelTypes {
    public final String Title;

    public HotelTypes(String title) {
        Title = title;
    }
}
