package com.eligasht.reservation.models.model.login;

/**
 * Created by elham.bonyani on 1/20/2018.
 */

public class Ticket {


    private String PassengerNameEn;
    public final String getPassengerNameEn()
    {
        return PassengerNameEn;
    }
    public final void setPassengerNameEn(String value)
    {
        PassengerNameEn = value;
    }


    private String TicketHTML;
    public final String getTicketHTML()
    {
        return TicketHTML;
    }
    public final void setTicketHTML(String value)
    {
        TicketHTML = value;
    }


    private String TicketID;
    public final String getTicketID()
    {
        return TicketID;
    }
    public final void setTicketID(String value)
    {
        TicketID = value;
    }


    private String TicketURL;
    public final String getTicketURL()
    {
        return TicketURL;
    }
    public final void setTicketURL(String value)
    {
        TicketURL = value;
    }

}
