package com.eligasht.reservation.models.hotel.api.flightchange;

/**
 * Created by Reza.nejati on 2/21/2018.
 */

public class LoadHotelFlightApiRespone {
    public final LoadFlightResult LoadFlightResult;

    public LoadHotelFlightApiRespone(com.eligasht.reservation.models.hotel.api.flightchange.LoadFlightResult loadFlightResult) {
        LoadFlightResult = loadFlightResult;
    }
}
