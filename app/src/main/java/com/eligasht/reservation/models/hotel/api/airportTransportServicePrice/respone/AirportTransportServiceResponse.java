package com.eligasht.reservation.models.hotel.api.airportTransportServicePrice.respone;

/**
 * Created by Reza.nejati on 3/28/2018.
 */

public class AirportTransportServiceResponse {
    public final AirportTransportServicePriceResult AirportTransportServicePriceResult;

    public AirportTransportServiceResponse(AirportTransportServicePriceResult airportTransportServicePriceResult) {
        AirportTransportServicePriceResult = airportTransportServicePriceResult;
    }
}
