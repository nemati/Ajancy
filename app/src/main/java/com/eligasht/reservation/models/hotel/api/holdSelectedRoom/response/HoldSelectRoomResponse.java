package com.eligasht.reservation.models.hotel.api.holdSelectedRoom.response;

/**
 * Created by Reza.nejati on 1/7/2018.
 */

public class HoldSelectRoomResponse {
    public final com.eligasht.reservation.models.hotel.api.holdSelectedRoom.response.HoldSelectedRoomResult HoldSelectedRoomResult;

    public HoldSelectRoomResponse(com.eligasht.reservation.models.hotel.api.holdSelectedRoom.response.HoldSelectedRoomResult holdSelectedRoomResult) {
        HoldSelectedRoomResult = holdSelectedRoomResult;
    }
}
