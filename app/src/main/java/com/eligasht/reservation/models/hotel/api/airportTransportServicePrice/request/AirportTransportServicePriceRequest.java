package com.eligasht.reservation.models.hotel.api.airportTransportServicePrice.request;

/**
 * Created by Reza.nejati on 2/13/2018.
 */

public class AirportTransportServicePriceRequest {
    public final AirportPriceRequest request;

    public AirportTransportServicePriceRequest(AirportPriceRequest request) {
        this.request = request;
    }
}
