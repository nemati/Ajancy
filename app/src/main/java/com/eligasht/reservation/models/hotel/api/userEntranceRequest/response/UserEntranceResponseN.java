package com.eligasht.reservation.models.hotel.api.userEntranceRequest.response;

/**
 * Created by Reza.nejati on 2/4/2018.
 */

public class UserEntranceResponseN {
    public final UserEntranceResponse MobileAppStartupServiceResult;

    public UserEntranceResponseN(UserEntranceResponse mobileAppStartupServiceResult) {
        MobileAppStartupServiceResult = mobileAppStartupServiceResult;
    }
}