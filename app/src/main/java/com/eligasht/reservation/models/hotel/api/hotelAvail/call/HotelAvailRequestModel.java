package com.eligasht.reservation.models.hotel.api.hotelAvail.call;

/**
 * Created by Reza.nejati on 1/5/2018.
 */

public class HotelAvailRequestModel {
  public final Request request;

    public HotelAvailRequestModel(Request request) {
        this.request = request;
    }
}
