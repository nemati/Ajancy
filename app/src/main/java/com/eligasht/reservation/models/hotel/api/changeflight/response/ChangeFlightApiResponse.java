package com.eligasht.reservation.models.hotel.api.changeflight.response;

/**
 * Created by Reza.nejati on 2/21/2018.
 */

public class ChangeFlightApiResponse {
    public final Object HotelPlusFlightChangeFltResult;

    public ChangeFlightApiResponse(Object hotelPlusFlightChangeFltResult) {
        HotelPlusFlightChangeFltResult = hotelPlusFlightChangeFltResult;
    }
}
