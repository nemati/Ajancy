package com.eligasht.reservation.models.hotel.api.detail;

/**
 * Created by Reza.nejati on 1/8/2018.
 */

public class GetHotelDetailResponse {
    public final com.eligasht.reservation.models.hotel.api.detail.GetHotelDetailResult GetHotelDetailResult;

    public GetHotelDetailResponse(com.eligasht.reservation.models.hotel.api.detail.GetHotelDetailResult getHotelDetailResult) {
        GetHotelDetailResult = getHotelDetailResult;
    }
}
