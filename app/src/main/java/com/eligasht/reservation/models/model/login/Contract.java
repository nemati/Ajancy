package com.eligasht.reservation.models.model.login;

/**
 * Created by elham.bonyani on 1/20/2018.
 */

public class Contract {

    private Integer AgcUserID = null;
    public final Integer getAgcUserID()
    {
        return AgcUserID;
    }
    public final void setAgcUserID(Integer value)
    {
        AgcUserID = value;
    }


    private String AgencyName;
    public final String getAgencyName()
    {
        return AgencyName;
    }
    public final void setAgencyName(String value)
    {
        AgencyName = value;
    }


    private String AgencyNameFa;
    public final String getAgencyNameFa()
    {
        return AgencyNameFa;
    }
    public final void setAgencyNameFa(String value)
    {
        AgencyNameFa = value;
    }


    private String Airline;
    public final String getAirline()
    {
        return Airline;
    }
    public final void setAirline(String value)
    {
        Airline = value;
    }


    private Integer BaxID =null;
    public final Integer getBaxID()
    {
        return BaxID;
    }
    public final void setBaxID(Integer value)
    {
        BaxID = value;
    }


    private String BaxName;
    public final String getBaxName()
    {
        return BaxName;
    }
    public final void setBaxName(String value)
    {
        BaxName = value;
    }
    private Integer CashierEventPriceBed = null;
    public final Integer getCashierEventPriceBed()
    {
        return CashierEventPriceBed;
    }
    public final void setCashierEventPriceBed(Integer value)
    {
        CashierEventPriceBed = value;
    }


    private Integer CashierEventPriceBes = null;
    public final Integer getCashierEventPriceBes()
    {
        return CashierEventPriceBes;
    }
    public final void setCashierEventPriceBes(Integer value)
    {
        CashierEventPriceBes = value;
    }


    private String CheckDate = null;
    public final String getCheckDate()
    {
        return CheckDate;
    }
    public final void setCheckDate(String value)
    {
        CheckDate = value;
    }


    private String CheckUserName;
    public final String getCheckUserName()
    {
        return CheckUserName;
    }
    public final void setCheckUserName(String value)
    {
        CheckUserName = value;
    }


    private String Checkin = null;
    public final String getCheckin()
    {
        return Checkin;
    }
    public final void setCheckin(String value)
    {
        Checkin = value;
    }


    private Integer CntAduQty = null;
    public final Integer getCntAduQty()
    {
        return CntAduQty;
    }
    public final void setCntAduQty(Integer value)
    {
        CntAduQty = value;
    }


    private Integer CntChd26Qty = null;
    public final Integer getCntChd26Qty()
    {
        return CntChd26Qty;
    }
    public final void setCntChd26Qty(Integer value)
    {
        CntChd26Qty = value;
    }


    private Integer CntChd612Qty = null;
    public final Integer getCntChd612Qty()
    {
        return CntChd612Qty;
    }
    public final void setCntChd612Qty(Integer value)
    {
        CntChd612Qty = value;
    }


    private Integer CntDiscount = null;
    public final Integer getCntDiscount()
    {
        return CntDiscount;
    }
    public final void setCntDiscount(Integer value)
    {
        CntDiscount = value;
    }


    private Integer CntDocDeliver = null;
    public final Integer getCntDocDeliver()
    {
        return CntDocDeliver;
    }
    public final void setCntDocDeliver(Integer value)
    {
        CntDocDeliver = value;
    }


    private String CntDocDeliverDate = null;
    public final String getCntDocDeliverDate()
    {
        return CntDocDeliverDate;
    }
    public final void setCntDocDeliverDate(String value)
    {
        CntDocDeliverDate = value;
    }


    private String CntDocDeliverDateFa;
    public final String getCntDocDeliverDateFa()
    {
        return CntDocDeliverDateFa;
    }
    public final void setCntDocDeliverDateFa(String value)
    {
        CntDocDeliverDateFa = value;
    }


    private Integer CntDocDeliverUserID = null;
    public final Integer getCntDocDeliverUserID()
    {
        return CntDocDeliverUserID;
    }
    public final void setCntDocDeliverUserID(Integer value)
    {
        CntDocDeliverUserID = value;
    }


    private int CntID;
    public final int getCntID()
    {
        return CntID;
    }
    public final void setCntID(int value)
    {
        CntID = value;
    }


    private Integer CntInfQty = null;
    public final Integer getCntInfQty()
    {
        return CntInfQty;
    }
    public final void setCntInfQty(Integer value)
    {
        CntInfQty = value;
    }


    private Integer CntPID =null;
    public final Integer getCntPID()
    {
        return CntPID;
    }
    public final void setCntPID(Integer value)
    {
        CntPID = value;
    }


    private String CntPartnerName;
    public final String getCntPartnerName()
    {
        return CntPartnerName;
    }
    public final void setCntPartnerName(String value)
    {
        CntPartnerName = value;
    }


    private Integer CntRevision = null;
    public final Integer getCntRevision()
    {
        return CntRevision;
    }
    public final void setCntRevision(Integer value)
    {
        CntRevision = value;
    }


    private Integer CntStatusTypeID = null;
    public final Integer getCntStatusTypeID()
    {
        return CntStatusTypeID;
    }
    public final void setCntStatusTypeID(Integer value)
    {
        CntStatusTypeID = value;
    }


    private Integer CntTotalPlusCom = null;
    public final Integer getCntTotalPlusCom()
    {
        return CntTotalPlusCom;
    }
    public final void setCntTotalPlusCom(Integer value)
    {
        CntTotalPlusCom = value;
    }


    private Integer CompanyID = null;
    public final Integer getCompanyID()
    {
        return CompanyID;
    }
    public final void setCompanyID(Integer value)
    {
        CompanyID = value;
    }


    private String CompanyName;
    public final String getCompanyName()
    {
        return CompanyName;
    }
    public final void setCompanyName(String value)
    {
        CompanyName = value;
    }


    private String ContractPrintURL;
    public final String getContractPrintURL()
    {
        return ContractPrintURL;
    }
    public final void setContractPrintURL(String value)
    {
        ContractPrintURL = value;
    }


    private String CustomerEmail;
    public final String getCustomerEmail()
    {
        return CustomerEmail;
    }
    public final void setCustomerEmail(String value)
    {
        CustomerEmail = value;
    }


    private String CustomerFax;
    public final String getCustomerFax()
    {
        return CustomerFax;
    }
    public final void setCustomerFax(String value)
    {
        CustomerFax = value;
    }


    private String CustomerMobile;
    public final String getCustomerMobile()
    {
        return CustomerMobile;
    }
    public final void setCustomerMobile(String value)
    {
        CustomerMobile = value;
    }


    private String CustomerName;
    public final String getCustomerName()
    {
        return CustomerName;
    }
    public final void setCustomerName(String value)
    {
        CustomerName = value;
    }


    private String CustomerPhone;
    public final String getCustomerPhone()
    {
        return CustomerPhone;
    }
    public final void setCustomerPhone(String value)
    {
        CustomerPhone = value;
    }


    private String Date = null;
    public final String getDate()
    {
        return Date;
    }
    public final void setDate(String value)
    {
        Date = value;
    }


    private String DateFa;
    public final String getDateFa()
    {
        return DateFa;
    }
    public final void setDateFa(String value)
    {
        DateFa = value;
    }


    private String DeliverUserName;
    public final String getDeliverUserName()
    {
        return DeliverUserName;
    }
    public final void setDeliverUserName(String value)
    {
        DeliverUserName = value;
    }


    private String Departure = null;
    public final String getDeparture()
    {
        return Departure;
    }
    public final void setDeparture(String value)
    {
        Departure = value;
    }


    private String Description;
    public final String getDescription()
    {
        return Description;
    }
    public final void setDescription(String value)
    {
        Description = value;
    }


    private Integer Discount = null;
    public final Integer getDiscount()
    {
        return Discount;
    }
    public final void setDiscount(Integer value)
    {
        Discount = value;
    }


    private String EditModifiedDate = null;
    public final String getEditModifiedDate()
    {
        return EditModifiedDate;
    }
    public final void setEditModifiedDate(String value)
    {
        EditModifiedDate = value;
    }
    private Integer EditStatusID = null;
    public final Integer getEditStatusID()
    {
        return EditStatusID;
    }
    public final void setEditStatusID(Integer value)
    {
        EditStatusID = value;
    }


    private String EditUserEn;
    public final String getEditUserEn()
    {
        return EditUserEn;
    }
    public final void setEditUserEn(String value)
    {
        EditUserEn = value;
    }


    private String EditUserFa;
    public final String getEditUserFa()
    {
        return EditUserFa;
    }
    public final void setEditUserFa(String value)
    {
        EditUserFa = value;
    }


    private String EditUserID;
    public final String getEditUserID()
    {
        return EditUserID;
    }
    public final void setEditUserID(String value)
    {
        EditUserID = value;
    }


    private String EditValidDate = null;
    public final String getEditValidDate()
    {
        return EditValidDate;
    }
    public final void setEditValidDate(String value)
    {
        EditValidDate = value;
    }


    private String EncryptedCnt_ID;
    public final String getEncryptedCnt_ID()
    {
        return EncryptedCnt_ID;
    }
    public final void setEncryptedCnt_ID(String value)
    {
        EncryptedCnt_ID = value;
    }


    private String EncryptedOldCnt_ID;
    public final String getEncryptedOldCnt_ID()
    {
        return EncryptedOldCnt_ID;
    }
    public final void setEncryptedOldCnt_ID(String value)
    {
        EncryptedOldCnt_ID = value;
    }


    private Integer FinEventPrice =null;
    public final Integer getFinEventPrice()
    {
        return FinEventPrice;
    }
    public final void setFinEventPrice(Integer value)
    {
        FinEventPrice = value;
    }


    private String FinEventSarfaslCode;
    public final String getFinEventSarfaslCode()
    {
        return FinEventSarfaslCode;
    }
    public final void setFinEventSarfaslCode(String value)
    {
        FinEventSarfaslCode = value;
    }


    private Integer FinalCom = null;
    public final Integer getFinalCom()
    {
        return FinalCom;
    }
    public final void setFinalCom(Integer value)
    {
        FinalCom = value;
    }


    private Integer FinalDiscount = null;
    public final Integer getFinalDiscount()
    {
        return FinalDiscount;
    }
    public final void setFinalDiscount(Integer value)
    {
        FinalDiscount = value;
    }


    private Integer FinalPrice = null;
    public final Integer getFinalPrice()
    {
        return FinalPrice;
    }
    public final void setFinalPrice(Integer value)
    {
        FinalPrice = value;
    }


    private String FincCenterID;
    public final String getFincCenterID()
    {
        return FincCenterID;
    }
    public final void setFincCenterID(String value)
    {
        FincCenterID = value;
    }


    private Integer FltType = null;
    public final Integer getFltType()
    {
        return FltType;
    }
    public final void setFltType(Integer value)
    {
        FltType = value;
    }


    private Integer FollowerID = null;
    public final Integer getFollowerID()
    {
        return FollowerID;
    }
    public final void setFollowerID(Integer value)
    {
        FollowerID = value;
    }


    private String FollowerName;
    public final String getFollowerName()
    {
        return FollowerName;
    }
    public final void setFollowerName(String value)
    {
        FollowerName = value;
    }


    private boolean HasFlt;
    public final boolean getHasFlt()
    {
        return HasFlt;
    }
    public final void setHasFlt(boolean value)
    {
        HasFlt = value;
    }


    private boolean HasHotel;
    public final boolean getHasHotel()
    {
        return HasHotel;
    }
    public final void setHasHotel(boolean value)
    {
        HasHotel = value;
    }


    private boolean HasIns;
    public final boolean getHasIns()
    {
        return HasIns;
    }
    public final void setHasIns(boolean value)
    {
        HasIns = value;
    }


    private boolean HasService;
    public final boolean getHasService()
    {
        return HasService;
    }
    public final void setHasService(boolean value)
    {
        HasService = value;
    }


    private boolean HasVisa;
    public final boolean getHasVisa()
    {
        return HasVisa;
    }
    public final void setHasVisa(boolean value)
    {
        HasVisa = value;
    }


    private Integer HotelConfirm = null;
    public final Integer getHotelConfirm()
    {
        return HotelConfirm;
    }
    public final void setHotelConfirm(Integer value)
    {
        HotelConfirm = value;
    }


    private Integer HotelIssue = null;
    public final Integer getHotelIssue()
    {
        return HotelIssue;
    }
    public final void setHotelIssue(Integer value)
    {
        HotelIssue = value;
    }


    private Integer HotelPenalty = null;
    public final Integer getHotelPenalty()
    {
        return HotelPenalty;
    }
    public final void setHotelPenalty(Integer value)
    {
        HotelPenalty = value;
    }


    private String InsurancePrintURL;
    public final String getInsurancePrintURL()
    {
        return InsurancePrintURL;
    }
    public final void setInsurancePrintURL(String value)
    {
        InsurancePrintURL = value;
    }


    private Integer Int = null;
    public final Integer getInt()
    {
        return Int;
    }
    public final void setInt(Integer value)
    {
        Int = value;
    }


    private Integer IsChecked = null;
    public final Integer getIsChecked()
    {
        return IsChecked;
    }
    public final void setIsChecked(Integer value)
    {
        IsChecked = value;
    }


    private Integer LoginTypeID = null;
    public final Integer getLoginTypeID()
    {
        return LoginTypeID;
    }
    public final void setLoginTypeID(Integer value)
    {
        LoginTypeID = value;
    }


    private String LoginTypeName;
    public final String getLoginTypeName()
    {
        return LoginTypeName;
    }
    public final void setLoginTypeName(String value)
    {
        LoginTypeName = value;
    }


    private String LoungePrintURL;
    public final String getLoungePrintURL()
    {
        return LoungePrintURL;
    }
    public final void setLoungePrintURL(String value)
    {
        LoungePrintURL = value;
    }


    private String ModifyDate = null;
    public final String getModifyDate()
    {
        return ModifyDate;
    }
    public final void setModifyDate(String value)
    {
        ModifyDate = value;
    }


    private String ModifyDateFa;
    public final String getModifyDateFa()
    {
        return ModifyDateFa;
    }
    public final void setModifyDateFa(String value)
    {
        ModifyDateFa = value;
    }


    private String ModifyUser;
    public final String getModifyUser()
    {
        return ModifyUser;
    }
    public final void setModifyUser(String value)
    {
        ModifyUser = value;
    }


    private Integer ModifyUserID =null;
    public final Integer getModifyUserID()
    {
        return ModifyUserID;
    }
    public final void setModifyUserID(Integer value)
    {
        ModifyUserID = value;
    }


    private Integer Note = null;
    public final Integer getNote()
    {
        return Note;
    }
    public final void setNote(Integer value)
    {
        Note = value;
    }


    private int OldCntID;
    public final int getOldCntID()
    {
        return OldCntID;
    }
    public final void setOldCntID(int value)
    {
        OldCntID = value;
    }


    private Integer OldCustomerID = null;
    public final Integer getOldCustomerID()
    {
        return OldCustomerID;
    }
    public final void setOldCustomerID(Integer value)
    {
        OldCustomerID = value;
    }


    private Integer OldStatusID = null;
    public final Integer getOldStatusID()
    {
        return OldStatusID;
    }
    public final void setOldStatusID(Integer value)
    {
        OldStatusID = value;
    }


    private String Origin;
    public final String getOrigin()
    {
        return Origin;
    }
    public final void setOrigin(String value)
    {
        Origin = value;
    }


    private String PackName;
    public final String getPackName()
    {
        return PackName;
    }
    public final void setPackName(String value)
    {
        PackName = value;
    }


    private String Package;
    public final String getPackage()
    {
        return Package;
    }
    public final void setPackage(String value)
    {
        Package = value;
    }


    private Long PageSize = null;
    public final Long getPageSize()
    {
        return PageSize;
    }
    public final void setPageSize(Long value)
    {
        PageSize = value;
    }


    private Integer Pardakhti = null;
    public final Integer getPardakhti()
    {
        return Pardakhti;
    }
    public final void setPardakhti(Integer value)
    {
        Pardakhti = value;
    }


    private Integer PartnerID = null;
    public final Integer getPartnerID()
    {
        return PartnerID;
    }
    public final void setPartnerID(Integer value)
    {
        PartnerID = value;
    }


    private String PathNames;
    public final String getPathNames()
    {
        return PathNames;
    }
    public final void setPathNames(String value)
    {
        PathNames = value;
    }


    private Integer Pax = null;
    public final Integer getPax()
    {
        return Pax;
    }
    public final void setPax(Integer value)
    {
        Pax = value;
    }


    private String PaymentURL;
    public final String getPaymentURL()
    {
        return PaymentURL;
    }
    public final void setPaymentURL(String value)
    {
        PaymentURL = value;
    }


    private Integer Penalty = null;
    public final Integer getPenalty()
    {
        return Penalty;
    }
    public final void setPenalty(Integer value)
    {
        Penalty = value;
    }


    private String RefNo;
    public final String getRefNo()
    {
        return RefNo;
    }
    public final void setRefNo(String value)
    {
        RefNo = value;
    }


    private Integer Remained = null;
    public final Integer getRemained()
    {
        return Remained;
    }
    public final void setRemained(Integer value)
    {
        Remained = value;
    }


    private String RemainedResult;
    public final String getRemainedResult()
    {
        return RemainedResult;
    }
    public final void setRemainedResult(String value)
    {
        RemainedResult = value;
    }


    private String RoomDescription;
    public final String getRoomDescription()
    {
        return RoomDescription;
    }
    public final void setRoomDescription(String value)
    {
        RoomDescription = value;
    }


    private Integer Rooms = null;
    public final Integer getRooms()
    {
        return Rooms;
    }
    public final void setRooms(Integer value)
    {
        Rooms = value;
    }


    private int RowNumber;
    public final int getRowNumber()
    {
        return RowNumber;
    }
    public final void setRowNumber(int value)
    {
        RowNumber = value;
    }


    private Integer RqBaseID = null;
    public final Integer getRqBaseID()
    {
        return RqBaseID;
    }
    public final void setRqBaseID(Integer value)
    {
        RqBaseID = value;
    }


    private String ServiceVoucherPrintURL;
    public final String getServiceVoucherPrintURL()
    {
        return ServiceVoucherPrintURL;
    }
    public final void setServiceVoucherPrintURL(String value)
    {
        ServiceVoucherPrintURL = value;
    }


    private String Status;
    public final String getStatus()
    {
        return Status;
    }
    public final void setStatus(String value)
    {
        Status = value;
    }


    private String StatusText;
    public final String getStatusText()
    {
        return StatusText;
    }
    public final void setStatusText(String value)
    {
        StatusText = value;
    }


    private String StatusTime = null;
    public final String getStatusTime()
    {
        return StatusTime;
    }
    public final void setStatusTime(String value)
    {
        StatusTime = value;
    }


    private String StatusTimeFa;
    public final String getStatusTimeFa()
    {
        return StatusTimeFa;
    }
    public final void setStatusTimeFa(String value)
    {
        StatusTimeFa = value;
    }


    private String StatusUser;
    public final String getStatusUser()
    {
        return StatusUser;
    }
    public final void setStatusUser(String value)
    {
        StatusUser = value;
    }


    private Integer StatusUserID = null;
    public final Integer getStatusUserID()
    {
        return StatusUserID;
    }
    public final void setStatusUserID(Integer value)
    {
        StatusUserID = value;
    }


    private Integer TaxPercent = null;
    public final Integer getTaxPercent()
    {
        return TaxPercent;
    }
    public final void setTaxPercent(Integer value)
    {
        TaxPercent = value;
    }


    private Integer TaxPrice = null;
    public final Integer getTaxPrice()
    {
        return TaxPrice;
    }
    public final void setTaxPrice(Integer value)
    {
        TaxPrice = value;
    }


    private Integer TicketConfirm = null;
    public final Integer getTicketConfirm()
    {
        return TicketConfirm;
    }
    public final void setTicketConfirm(Integer value)
    {
        TicketConfirm = value;
    }


    private Integer TicketDescription = null;
    public final Integer getTicketDescription()
    {
        return TicketDescription;
    }
    public final void setTicketDescription(Integer value)
    {
        TicketDescription = value;
    }


    private Integer TicketIssue = null;
    public final Integer getTicketIssue()
    {
        return TicketIssue;
    }
    public final void setTicketIssue(Integer value)
    {
        TicketIssue = value;
    }


    private TTicket Tickets;
    public final TTicket getTickets()
    {
        return Tickets;
    }
    public final void setTickets(TTicket value)
    {
        Tickets = value;
    }


    private Integer TktQty = null;
    public final Integer getTktQty()
    {
        return TktQty;
    }
    public final void setTktQty(Integer value)
    {
        TktQty = value;
    }


    private Integer TotalCommision = null;
    public final Integer getTotalCommision()
    {
        return TotalCommision;
    }
    public final void setTotalCommision(Integer value)
    {
        TotalCommision = value;
    }


    private Integer TotalPrice = null;
    public final Integer getTotalPrice()
    {
        return TotalPrice;
    }
    public final void setTotalPrice(Integer value)
    {
        TotalPrice = value;
    }


    private String Type;
    public final String getType()
    {
        return Type;
    }
    public final void setType(String value)
    {
        Type = value;
    }


    private Integer VisaConfirm = null;
    public final Integer getVisaConfirm()
    {
        return VisaConfirm;
    }
    public final void setVisaConfirm(Integer value)
    {
        VisaConfirm = value;
    }


    private String VisaDescription;
    public final String getVisaDescription()
    {
        return VisaDescription;
    }
    public final void setVisaDescription(String value)
    {
        VisaDescription = value;
    }


    private Integer VisaIssue = null;
    public final Integer getVisaIssue()
    {
        return VisaIssue;
    }
    public final void setVisaIssue(Integer value)
    {
        VisaIssue = value;
    }


    private String VisaPrintURL;
    public final String getVisaPrintURL()
    {
        return VisaPrintURL;
    }
    public final void setVisaPrintURL(String value)
    {
        VisaPrintURL = value;
    }


    private Integer VisaQty = null;
    public final Integer getVisaQty()
    {
        return VisaQty;
    }
    public final void setVisaQty(Integer value)
    {
        VisaQty = value;
    }


    private String VoucherPrintURL;
    public final String getVoucherPrintURL()
    {
        return VoucherPrintURL;
    }
    public final void setVoucherPrintURL(String value)
    {
        VoucherPrintURL = value;
    }


    private Integer Web = null;
    public final Integer getWeb()
    {
        return Web;
    }
    public final void setWeb(Integer value)
    {
        Web = value;
    }


    private Integer WebEndUser = null;
    public final Integer getWebEndUser()
    {
        return WebEndUser;
    }
    public final void setWebEndUser(Integer value)
    {
        WebEndUser = value;
    }


    private Integer WebUserID = null;
    public final Integer getWebUserID()
    {
        return WebUserID;
    }
    public final void setWebUserID(Integer value)
    {
        WebUserID = value;
    }


    private String WebUserName;
    public final String getWebUserName()
    {
        return WebUserName;
    }
    public final void setWebUserName(String value)
    {
        WebUserName = value;
    }


}
