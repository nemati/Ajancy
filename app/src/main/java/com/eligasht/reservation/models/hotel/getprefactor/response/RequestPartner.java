package com.eligasht.reservation.models.hotel.getprefactor.response;

/**
 * Created by Reza.nejati on 1/31/2018.
 */

public class RequestPartner {
    public final String RqPartner_Email;

    public RequestPartner(String rqPartner_Email) {
        RqPartner_Email = rqPartner_Email;
    }
}
