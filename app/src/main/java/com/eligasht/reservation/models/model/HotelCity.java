package com.eligasht.reservation.models.model;

public class HotelCity {

private String CityCode;
private int CityID;
private String CityNameEn;
private String CityNameFa;
private int CountryID;
public String getCityCode() {
	return CityCode;
}
public void setCityCode(String cityCode) {
	CityCode = cityCode;
}
public int getCityID() {
	return CityID;
}
public void setCityID(int cityID) {
	CityID = cityID;
}
public String getCityNameEn() {
	return CityNameEn;
}
public void setCityNameEn(String cityNameEn) {
	CityNameEn = cityNameEn;
}
public String getCityNameFa() {
	return CityNameFa;
}
public void setCityNameFa(String cityNameFa) {
	CityNameFa = cityNameFa;
}
public int getCountryID() {
	return CountryID;
}
public void setCountryID(int countryID) {
	CountryID = countryID;
}

}
