package com.eligasht.reservation.models.hotel.api.hotelName.response;

/**
 * Created by Reza.nejati on 2/13/2018.
 */

public class HotelNameApiResponse {
    public final GetHotelAjaxResult GetHotelAjaxResult;

    public HotelNameApiResponse(com.eligasht.reservation.models.hotel.api.hotelName.response.GetHotelAjaxResult getHotelAjaxResult) {
        GetHotelAjaxResult = getHotelAjaxResult;
    }
}
