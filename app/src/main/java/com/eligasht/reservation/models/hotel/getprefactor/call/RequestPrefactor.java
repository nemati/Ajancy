package com.eligasht.reservation.models.hotel.getprefactor.call;

/**
 * Created by Reza.nejati on 1/28/2018.
 */

public class RequestPrefactor {
    public final RequestPre request;

    public RequestPrefactor(RequestPre request) {
        this.request = request;
    }
}
