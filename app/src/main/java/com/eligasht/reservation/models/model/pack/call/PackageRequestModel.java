package com.eligasht.reservation.models.model.pack.call;

/**
 * Created by elham.bonyani on 1/7/2018.
 */

public class PackageRequestModel {
    public final PackageListReq request;

    public PackageRequestModel(PackageListReq request) {
        this.request = request;
    }
}
