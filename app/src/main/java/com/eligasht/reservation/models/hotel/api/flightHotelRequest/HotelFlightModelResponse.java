package com.eligasht.reservation.models.hotel.api.flightHotelRequest;

/**
 * Created by Reza.nejati on 1/16/2018.
 */

public class HotelFlightModelResponse {
    public final HotelFlightSearchResult HotelFlightSearchResult;


    public HotelFlightModelResponse(HotelFlightSearchResult hotelFlightSearchResult) {
        HotelFlightSearchResult = hotelFlightSearchResult;
    }
}
