package com.eligasht.reservation.models.model.pack.call;


import com.eligasht.reservation.models.hotel.api.hotelAvail.call.Identity;

/**
 * Created by elham.bonydni on 1/6/18.
 */

public class CityListRq {
    private Identity identity;

    public CityListRq(Identity identity) {
        this.identity = identity;
    }
}
