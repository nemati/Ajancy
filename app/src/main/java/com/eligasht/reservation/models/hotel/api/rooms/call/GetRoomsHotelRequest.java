package com.eligasht.reservation.models.hotel.api.rooms.call;

public class GetRoomsHotelRequest {
    public final RoomRequest request;
    public GetRoomsHotelRequest(RoomRequest request) {
        this.request = request;
    }
}
