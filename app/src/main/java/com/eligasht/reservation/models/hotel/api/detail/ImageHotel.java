package com.eligasht.reservation.models.hotel.api.detail;

/**
 * Created by Reza.nejati on 1/8/2018.
 */

public class ImageHotel {
    public final String HotelImagesURL;


    public ImageHotel(String hotelImagesURL) {
        HotelImagesURL = hotelImagesURL;
    }
}
