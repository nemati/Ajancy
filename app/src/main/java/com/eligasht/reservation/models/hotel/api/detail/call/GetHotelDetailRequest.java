package com.eligasht.reservation.models.hotel.api.detail.call;

/**
 * Created by Reza.nejati on 1/8/2018.
 */

public class GetHotelDetailRequest {
    public final GetHotelDRequest request;

    public GetHotelDetailRequest(GetHotelDRequest request) {
        this.request = request;
    }
}
