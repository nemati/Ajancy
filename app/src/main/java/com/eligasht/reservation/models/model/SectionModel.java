package com.eligasht.reservation.models.model;

/**
 * Created by Mahsa.azizi on 1/15/2018.
 */

public class SectionModel {

    private String SectionName;
    private String Description ;
    private String ImageAddress;
    private String IconName;
    private String IconCode ;

    public String getSectionName() {
        return SectionName;
    }

    public void setSectionName(String sectionName) {
        SectionName = sectionName;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getImageAddress() {
        return ImageAddress;
    }

    public void setImageAddress(String imageAddress) {
        ImageAddress = imageAddress;
    }

    public String getIconName() {
        return IconName;
    }

    public void setIconName(String iconName) {
        IconName = iconName;
    }

    public String getIconCode() {
        return IconCode;
    }

    public void setIconCode(String iconCode) {
        IconCode = iconCode;
    }


}
