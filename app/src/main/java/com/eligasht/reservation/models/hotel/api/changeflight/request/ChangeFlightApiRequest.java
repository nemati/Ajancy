package com.eligasht.reservation.models.hotel.api.changeflight.request;

/**
 * Created by Reza.nejati on 2/21/2018.
 */

public class ChangeFlightApiRequest {
    public final Request request;

    public ChangeFlightApiRequest(Request request) {
        this.request = request;
    }
}
