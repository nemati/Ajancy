package com.eligasht.service.model.test;

/**
 * Created by Ahmad.nemati on 4/23/2018.
 */
public class TransferClickEvenBus {
    int index;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public TransferClickEvenBus(int index) {

        this.index = index;
    }
}
