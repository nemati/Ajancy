
package com.eligasht.service.model.XPackage.response.GetPreFactorDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FactorSummary {

    @SerializedName("AdultCount")
    @Expose
    private Integer adultCount;
    @SerializedName("AdultTotalPrice")
    @Expose
    private Integer adultTotalPrice;
    @SerializedName("ChildCount")
    @Expose
    private Integer childCount;
    @SerializedName("ChildTotalPrice")
    @Expose
    private Integer childTotalPrice;
    @SerializedName("ContractNo")
    @Expose
    private String contractNo;
    @SerializedName("Discount")
    @Expose
    private Integer discount;
    @SerializedName("EncryptedContractNo")
    @Expose
    private String encryptedContractNo;
    @SerializedName("EncryptedRqBase_ID")
    @Expose
    private String encryptedRqBaseID;
    @SerializedName("FactorTitle")
    @Expose
    private String factorTitle;
    @SerializedName("FinalPrice")
    @Expose
    private Integer finalPrice;
    @SerializedName("FollowerEn")
    @Expose
    private String followerEn;
    @SerializedName("FollowerFa")
    @Expose
    private String followerFa;
    @SerializedName("FollowerIntTel")
    @Expose
    private String followerIntTel;
    @SerializedName("InfantCount")
    @Expose
    private Integer infantCount;
    @SerializedName("InfantTotalPrice")
    @Expose
    private Integer infantTotalPrice;
    @SerializedName("IsRegistered")
    @Expose
    private Boolean isRegistered;
    @SerializedName("OnlinePaymentReturnURL")
    @Expose
    private Object onlinePaymentReturnURL;
    @SerializedName("OnlinePaymentURL")
    @Expose
    private String onlinePaymentURL;
    @SerializedName("PassTotalClubDiscount")
    @Expose
    private Integer passTotalClubDiscount;
    @SerializedName("PassTotalCom")
    @Expose
    private Integer passTotalCom;
    @SerializedName("PassTotalDiscount")
    @Expose
    private Integer passTotalDiscount;
    @SerializedName("PassTotalSpecialDiscount")
    @Expose
    private Integer passTotalSpecialDiscount;
    @SerializedName("PromoCode")
    @Expose
    private Object promoCode;
    @SerializedName("PromoDisount")
    @Expose
    private Integer promoDisount;
    @SerializedName("PurchaseDate")
    @Expose
    private String purchaseDate;
    @SerializedName("PurchaseDateFa")
    @Expose
    private String purchaseDateFa;
    @SerializedName("PurchaseType")
    @Expose
    private Integer purchaseType;
    @SerializedName("PurchaseTypeEn")
    @Expose
    private String purchaseTypeEn;
    @SerializedName("RegisterDate")
    @Expose
    private String registerDate;
    @SerializedName("RegisterUserFa")
    @Expose
    private String registerUserFa;
    @SerializedName("RqBaseCategory")
    @Expose
    private Integer rqBaseCategory;
    @SerializedName("RqBase_ID")
    @Expose
    private Integer rqBaseID;
    @SerializedName("TotalCom")
    @Expose
    private Integer totalCom;
    @SerializedName("TotalDiscount")
    @Expose
    private Integer totalDiscount;
    @SerializedName("TotalFlight")
    @Expose
    private Integer totalFlight;
    @SerializedName("TotalHotel")
    @Expose
    private Integer totalHotel;
    @SerializedName("TotalPlusCom")
    @Expose
    private Integer totalPlusCom;
    @SerializedName("TotalPrice")
    @Expose
    private Integer totalPrice;
    @SerializedName("UniqueID")
    @Expose
    private Object uniqueID;

    public Integer getAdultCount() {
        return adultCount;
    }

    public void setAdultCount(Integer adultCount) {
        this.adultCount = adultCount;
    }

    public Integer getAdultTotalPrice() {
        return adultTotalPrice;
    }

    public void setAdultTotalPrice(Integer adultTotalPrice) {
        this.adultTotalPrice = adultTotalPrice;
    }

    public Integer getChildCount() {
        return childCount;
    }

    public void setChildCount(Integer childCount) {
        this.childCount = childCount;
    }

    public Integer getChildTotalPrice() {
        return childTotalPrice;
    }

    public void setChildTotalPrice(Integer childTotalPrice) {
        this.childTotalPrice = childTotalPrice;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public String getEncryptedContractNo() {
        return encryptedContractNo;
    }

    public void setEncryptedContractNo(String encryptedContractNo) {
        this.encryptedContractNo = encryptedContractNo;
    }

    public String getEncryptedRqBaseID() {
        return encryptedRqBaseID;
    }

    public void setEncryptedRqBaseID(String encryptedRqBaseID) {
        this.encryptedRqBaseID = encryptedRqBaseID;
    }

    public String getFactorTitle() {
        return factorTitle;
    }

    public void setFactorTitle(String factorTitle) {
        this.factorTitle = factorTitle;
    }

    public Integer getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(Integer finalPrice) {
        this.finalPrice = finalPrice;
    }

    public String getFollowerEn() {
        return followerEn;
    }

    public void setFollowerEn(String followerEn) {
        this.followerEn = followerEn;
    }

    public String getFollowerFa() {
        return followerFa;
    }

    public void setFollowerFa(String followerFa) {
        this.followerFa = followerFa;
    }

    public String getFollowerIntTel() {
        return followerIntTel;
    }

    public void setFollowerIntTel(String followerIntTel) {
        this.followerIntTel = followerIntTel;
    }

    public Integer getInfantCount() {
        return infantCount;
    }

    public void setInfantCount(Integer infantCount) {
        this.infantCount = infantCount;
    }

    public Integer getInfantTotalPrice() {
        return infantTotalPrice;
    }

    public void setInfantTotalPrice(Integer infantTotalPrice) {
        this.infantTotalPrice = infantTotalPrice;
    }

    public Boolean getIsRegistered() {
        return isRegistered;
    }

    public void setIsRegistered(Boolean isRegistered) {
        this.isRegistered = isRegistered;
    }

    public Object getOnlinePaymentReturnURL() {
        return onlinePaymentReturnURL;
    }

    public void setOnlinePaymentReturnURL(Object onlinePaymentReturnURL) {
        this.onlinePaymentReturnURL = onlinePaymentReturnURL;
    }

    public String getOnlinePaymentURL() {
        return onlinePaymentURL;
    }

    public void setOnlinePaymentURL(String onlinePaymentURL) {
        this.onlinePaymentURL = onlinePaymentURL;
    }

    public Integer getPassTotalClubDiscount() {
        return passTotalClubDiscount;
    }

    public void setPassTotalClubDiscount(Integer passTotalClubDiscount) {
        this.passTotalClubDiscount = passTotalClubDiscount;
    }

    public Integer getPassTotalCom() {
        return passTotalCom;
    }

    public void setPassTotalCom(Integer passTotalCom) {
        this.passTotalCom = passTotalCom;
    }

    public Integer getPassTotalDiscount() {
        return passTotalDiscount;
    }

    public void setPassTotalDiscount(Integer passTotalDiscount) {
        this.passTotalDiscount = passTotalDiscount;
    }

    public Integer getPassTotalSpecialDiscount() {
        return passTotalSpecialDiscount;
    }

    public void setPassTotalSpecialDiscount(Integer passTotalSpecialDiscount) {
        this.passTotalSpecialDiscount = passTotalSpecialDiscount;
    }

    public Object getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(Object promoCode) {
        this.promoCode = promoCode;
    }

    public Integer getPromoDisount() {
        return promoDisount;
    }

    public void setPromoDisount(Integer promoDisount) {
        this.promoDisount = promoDisount;
    }

    public String getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public String getPurchaseDateFa() {
        return purchaseDateFa;
    }

    public void setPurchaseDateFa(String purchaseDateFa) {
        this.purchaseDateFa = purchaseDateFa;
    }

    public Integer getPurchaseType() {
        return purchaseType;
    }

    public void setPurchaseType(Integer purchaseType) {
        this.purchaseType = purchaseType;
    }

    public String getPurchaseTypeEn() {
        return purchaseTypeEn;
    }

    public void setPurchaseTypeEn(String purchaseTypeEn) {
        this.purchaseTypeEn = purchaseTypeEn;
    }

    public String getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(String registerDate) {
        this.registerDate = registerDate;
    }

    public String getRegisterUserFa() {
        return registerUserFa;
    }

    public void setRegisterUserFa(String registerUserFa) {
        this.registerUserFa = registerUserFa;
    }

    public Integer getRqBaseCategory() {
        return rqBaseCategory;
    }

    public void setRqBaseCategory(Integer rqBaseCategory) {
        this.rqBaseCategory = rqBaseCategory;
    }

    public Integer getRqBaseID() {
        return rqBaseID;
    }

    public void setRqBaseID(Integer rqBaseID) {
        this.rqBaseID = rqBaseID;
    }

    public Integer getTotalCom() {
        return totalCom;
    }

    public void setTotalCom(Integer totalCom) {
        this.totalCom = totalCom;
    }

    public Integer getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(Integer totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public Integer getTotalFlight() {
        return totalFlight;
    }

    public void setTotalFlight(Integer totalFlight) {
        this.totalFlight = totalFlight;
    }

    public Integer getTotalHotel() {
        return totalHotel;
    }

    public void setTotalHotel(Integer totalHotel) {
        this.totalHotel = totalHotel;
    }

    public Integer getTotalPlusCom() {
        return totalPlusCom;
    }

    public void setTotalPlusCom(Integer totalPlusCom) {
        this.totalPlusCom = totalPlusCom;
    }

    public Integer getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Integer totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Object getUniqueID() {
        return uniqueID;
    }

    public void setUniqueID(Object uniqueID) {
        this.uniqueID = uniqueID;
    }

}
