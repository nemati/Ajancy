
package com.eligasht.service.model.hotel.room.response;

import java.util.List;

import com.eligasht.service.model.BaseModel;
import com.eligasht.service.model.error.Error;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetRoomsListResult extends BaseModel {

    @SerializedName("Comments")
    @Expose
    private Object comments;

    @SerializedName("ResultKey")
    @Expose
    private Object resultKey;
    @SerializedName("Warningss")
    @Expose
    private Object warningss;
    @SerializedName("SearchKey")
    @Expose
    private String searchKey;
    @SerializedName("roomList")
    @Expose
    private List<RoomList> roomList = null;

    public Object getComments() {
        return comments;
    }

    public void setComments(Object comments) {
        this.comments = comments;
    }



    public Object getResultKey() {
        return resultKey;
    }

    public void setResultKey(Object resultKey) {
        this.resultKey = resultKey;
    }

    public Object getWarningss() {
        return warningss;
    }

    public void setWarningss(Object warningss) {
        this.warningss = warningss;
    }

    public String getSearchKey() {
        return searchKey;
    }

    public void setSearchKey(String searchKey) {
        this.searchKey = searchKey;
    }

    public List<RoomList> getRoomList() {
        return roomList;
    }

    public void setRoomList(List<RoomList> roomList) {
        this.roomList = roomList;
    }

}
